using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Database;
using Database.Model;
using Database.Repositories.DeportesRepository;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Service.DtoModels.Deportes;
using Service.Services.DeportesServices;
using Tests.Confing;
using TestSupport.EfHelpers;
using Xunit;

namespace Tests
{
    public class DeportesServicesTest
    {
        private readonly IMapper _mapper = null;

        public DeportesServicesTest()
        {
            _mapper = AutoMapperConfigTest.GetImapper();
        }

        [Fact]
        public async Task GetDeporte_Returns1()
        {
            List<Deportes> listItems = GetDeportesTesDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IDeportesServices deportesService = InjecDeporteService(context);
                DtoDeportes items = await deportesService.GetDeporte(1);

                items.Nombre.Should().Be("Tenis");
            }
        }

        [Fact]
        public async Task GetDeporte_NoExisting_Returns0()
        {
            List<Deportes> listItems = GetDeportesTesDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IDeportesServices deportesService = InjecDeporteService(context);
                DtoDeportes item = await deportesService.GetDeporte(5);

                item.Should().BeNull();
            }
        }

        [Fact]
        public async Task GetDeportes_AllDeportes_Returns4()
        {
            List<Deportes> listItems = GetDeportesTesDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IDeportesServices deportesService = InjecDeporteService(context);
                List<DtoDeportes> items = await deportesService.GetDeportes(null, null);

                items.Should().HaveCount(4);
            }
        }

        [Fact]
        public async Task AddDeporte_NewDeporte_Returns1Inserted()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                context.CreateEmptyViaWipe();

                IDeportesServices deportesService = InjecDeporteService(context);

                DtoDeportesEdit item = new DtoDeportesEdit { Nombre = "Padel", NumeroJugadores = 2 };

                await deportesService.AddDeporte(item);

                context.Deportes.FirstOrDefault().Should().NotBeNull();
            }
        }

        [Fact]
        public async Task UpdateDeporte1_ReturnsNewNombre()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                List<Deportes> listItems = GetDeportesTesDb();
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);

                IDeportesServices deportesService = InjecDeporteService(context);

                listItems[0].Nombre = "Test";

                DtoDeportes itemUpdated = _mapper.Map<DtoDeportes>(listItems[0]);
                await deportesService.UpdateDeporte(itemUpdated);

                context.Deportes.FirstOrDefault().Should().NotBeNull();
                context.Deportes.First().Nombre.Should().Be("Test");
            }
        }

        [Fact]
        public async Task DeleteDeportes1_ReturnsNull()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                List<Deportes> listItems = new List<Deportes> { new Deportes { Id = 1, Nombre = "Tenis", NumeroJugadores = 2 } };
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);

                IDeportesServices deportesService = InjecDeporteService(context);

                await deportesService.DeleteDeporte(1);

                context.Deportes.FirstOrDefault().Should().BeNull();
            }
        }

        #region "M�todos comunes"

        private List<Deportes> GetDeportesTesDb()
        {
            List<Deportes> itemDeportes = new List<Deportes>
            {
                new Deportes{Id = 1, Nombre= "Tenis", NumeroJugadores = 2},
                new Deportes{Id = 2, Nombre= "F�tbol", NumeroJugadores = 22},
                new Deportes{Id = 3, Nombre= "F�tbol Sala", NumeroJugadores =  10},
                new Deportes{Id = 4, Nombre= "Baloncesto", NumeroJugadores =  10},
            };

            return itemDeportes;
        }

        private IDeportesServices InjecDeporteService(ClubDeportivoContext context)
        {
            IDeportesRepository repository = new DeportesRepository(context);
            IDeportesServices service = new DeportesServices (repository, _mapper);
            return service;
        }

        #endregion
    }
}
