using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Database;
using Database.Model;
using Database.Repositories.DeportesRepository;
using Database.Repositories.PistaRepository;
using Database.Repositories.ReservaRepository;
using Database.Repositories.SocioRepository;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Service.DtoModels.Deportes;
using Service.DtoModels.Pista;
using Service.DtoModels.Socio;
using Service.Services.DeportesServices;
using Service.Services.PistaServices;
using Service.Services.ReservaServices;
using Service.Services.SocioServices;
using Tests.Confing;
using TestSupport.EfHelpers;
using Xunit;

namespace Tests
{
    public class SocioServicesTest
    {
        private readonly IMapper _mapper = null;

        public SocioServicesTest()
        {
            _mapper = AutoMapperConfigTest.GetImapper();
        }

        [Fact]
        public async Task GetSocio1_Returns1()
        {
            List<Socio> listItems = GetSocioTestDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                ISocioServices socioService = InjecSocioService(context);
                DtoSocio item = await socioService.GetSocio(1);

                item.Nombre.Should().Be("David");
            }
        }

        [Fact]
        public async Task GetSocio_NoExisting_Returns0()
        {
            List<Socio> listItems = GetSocioTestDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                ISocioServices socioService = InjecSocioService(context);
                DtoSocio item = await socioService.GetSocio(5);

                item.Should().BeNull();
            }
        }

        [Fact]
        public async Task GetSocios_AllSocios_Returns3()
        {
            List<Socio> listItems = GetSocioTestDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                ISocioServices socioService = InjecSocioService(context);
                List<DtoSocio> items = await socioService.GetSocios(null, null);

                items.Should().HaveCount(3);
            }
        }

        [Fact]
        public async Task AddSocio_NewSocio_Returns1Inserted()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                Usuario itemUsuario1 = new Usuario
                {
                    Id = 1,
                    Nombre = "David",
                    Password = "test1",
                    Email = "email1@email.com",
                    Telefono = "+34968555555",
                    Borrado = false
                };

                await ContextConfig.SeedRangeDatabaseSameContext(context, new List<Usuario> { itemUsuario1 });

                ISocioServices socioService = InjecSocioService(context);

                DtoSocioAdd item = new DtoSocioAdd
                {
                    Nombre = "Test",
                    CodigoPostal = "30110",
                    Direccion = "Dirreccion 1",
                    Email = "email1@email.com",
                    IdUsuario = 1,
                    Poblacion = "Poblacion 1",
                    Telefono = "+34615489562"
                };

                await socioService.AddSocio(item);

                context.Socio.FirstOrDefault().Should().NotBeNull();
            }
        }

        [Fact]
        public async Task UpdateSocio1_ReturnsNewNombre()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                List<Socio> listItems = GetSocioTestDb();
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);

                ISocioServices socioService = InjecSocioService(context);

                listItems[0].Nombre = "Test";

                DtoSocioUpdate itemUpdated = _mapper.Map<DtoSocioUpdate>(listItems[0]);
                await socioService.UpdateSocio(itemUpdated);

                context.Socio.FirstOrDefault().Should().NotBeNull();
                context.Socio.First().Nombre.Should().Be("Test");
            }
        }

        [Fact]
        public async Task DeleteSocio1_ReturnsNull()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                Usuario itemUsuario1 = new Usuario
                {
                    Id = 1,
                    Nombre = "David",
                    Password = "test1",
                    Email = "email1@email.com",
                    Telefono = "+34968555555",
                    Borrado = false
                };

                List<Socio> listItems = new List<Socio>
                {
                    new Socio
                    {
                        Id = 1,
                        Borrado = false,
                        CodigoPostal = "30110",
                        Direccion = "Dirreccion 1",
                        Email = "test@email.com",
                        IdUsuario = 1,
                        Nombre = "David",
                        Poblacion = "Población 1",
                        Telefono = "968777777",
                        UsuarioNavigation = itemUsuario1
                    }
                };
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);

                ISocioServices socioService = InjecSocioService(context);

                await socioService.DeleteSocio(1);

                context.Socio.FirstOrDefault().Should().BeNull();
            }
        }

        #region "Métodos comunes"

        private List<Socio> GetSocioTestDb()
        {
            Usuario itemUsuario1 = new Usuario
            {
                Id = 1,
                Nombre = "David",
                Password = "test1",
                Email = "email1@email.com",
                Telefono = "+34968555555",
                Borrado = false
            };
            Usuario itemUsuario2 = new Usuario
            {
                Id = 2,
                Nombre = "Pepe",
                Password = "test2",
                Email = "email2@email.com",
                Telefono = "+34968777777",
                Borrado = false
            };

            Usuario itemUsuario3 = new Usuario
            {
                Id = 3,
                Nombre = "Juan",
                Password = "test3",
                Email = "email3@email.com",
                Telefono = "+34968888888",
                Borrado = false
            };

            List<Socio> itemPistas = new List<Socio>
            {
                new Socio
                {
                    Id = 1,
                    Borrado = false,
                    CodigoPostal = "30110",
                    Direccion = "Dirreccion 1",
                    Email = "test@email.com",
                    IdUsuario = 1,
                    Nombre = "David",
                    Poblacion = "Población 1",
                    Telefono = "968777777",
                    UsuarioNavigation = itemUsuario1
                },
                new Socio
                {
                    Id = 2,
                    Borrado = false,
                    CodigoPostal = "30111",
                    Direccion = "Dirreccion 2",
                    Email = "test2@email.com",
                    IdUsuario = 2,
                    Nombre = "Pepe",
                    Poblacion = "Población 2",
                    Telefono = "968888888",
                    UsuarioNavigation = itemUsuario2
                },
                new Socio
                {
                    Id = 3,
                    Borrado = false,
                    CodigoPostal = "30112",
                    Direccion = "Dirreccion 3",
                    Email = "test3@email.com",
                    IdUsuario = 3,
                    Nombre = "Juan",
                    Poblacion = "Población 3",
                    Telefono = "968999999",
                    UsuarioNavigation = itemUsuario3
                }
            };

            return itemPistas;
        }

        private ISocioServices InjecSocioService(ClubDeportivoContext context)
        {
            ISocioRepository repository = new SocioRepository(context);
            ISocioServices service = new SocioServices(repository, _mapper);
            return service;
        }

        #endregion
    }
}
