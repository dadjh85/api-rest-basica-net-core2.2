using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Database;
using Database.Model;
using Database.Repositories.DeportesRepository;
using Database.Repositories.PistaRepository;
using Database.Repositories.ReservaRepository;
using Database.Repositories.SocioRepository;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Service.DtoModels.Deportes;
using Service.DtoModels.Pista;
using Service.DtoModels.Reserva;
using Service.DtoModels.Socio;
using Service.Services.DeportesServices;
using Service.Services.PistaServices;
using Service.Services.ReservaServices;
using Service.Services.SocioServices;
using Tests.Confing;
using TestSupport.EfHelpers;
using Xunit;

namespace Tests
{
    public class ReservaServicesTest
    {
        private readonly IMapper _mapper = null;

        public ReservaServicesTest()
        {
            _mapper = AutoMapperConfigTest.GetImapper();
        }

        [Fact]
        public async Task GetReserva1_Returns1()
        {
            List<Reserva> listItems = GetReservasTestDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IReservaServices reservaService = InjecReservasService(context);
                DtoReserva item = await reservaService.GetReserva(1);

                item.IdUsuario.Should().Be(1);
            }
        }

        [Fact]
        public async Task GeReserva_NoExisting_Returns0()
        {
            List<Reserva> listItems = GetReservasTestDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IReservaServices reservaService = InjecReservasService(context);
                DtoReserva item = await reservaService.GetReserva(5);

                item.Should().BeNull();
            }
        }

        [Fact]
        public async Task GetReserva_AllReservas_Returns2()
        {
            List<Reserva> listItems = GetReservasTestDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IReservaServices reservaService = InjecReservasService(context);
                List<DtoReserva> items = await reservaService.GetReservas(null, null);

                items.Should().HaveCount(2);
            }
        }

        [Fact]
        public async Task AddReserva_NewReserva_Returns1Inserted()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                List<Reserva> listItems = GetReservasTestDb();
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IReservaServices reservaService = InjecReservasService(context);

                DtoReservaAdd item = new DtoReservaAdd
                {
                    Fecha = new DateTime(2019, 06, 24, 11, 0, 0),
                    IdUsuario = 1,
                    IdPista = 1,
                    IdSocio = 1,
                    Observaciones = "Test"
                };

                await reservaService.AddReserva(item);

                context.Reserva.FirstOrDefault().Should().NotBeNull();
            }
        }

        [Fact]
        public async Task UpdateReserva1_ReturnsNewObservaciones()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                List<Reserva> listItems = GetReservasTestDb();
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);

                IReservaServices reservaService = InjecReservasService(context);

                listItems[0].Observaciones = "TestNew";

                DtoReservaUpdate itemUpdated = _mapper.Map<DtoReservaUpdate>(listItems[0]);
                await reservaService.UpdateReserva(itemUpdated);

                context.Reserva.FirstOrDefault().Should().NotBeNull();
                context.Reserva.First().Observaciones.Should().Be("TestNew");
            }
        }

        [Fact]
        public async Task DeleterReserva1_ReturnsNull()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                Reserva itemReserva = GetItemReserva();

                await ContextConfig.SeedRangeDatabaseSameContext(context, new List<Reserva> { itemReserva });

                IReservaServices reservaService = InjecReservasService(context);

                await reservaService.DeleteReserva(1);

                context.Reserva.FirstOrDefault().Should().BeNull();
            }
        }

        #region "Métodos comunes"

        private Pista GetItemPista()
        {
            Deportes itemDeporte1 = new Deportes
            {
                Id = 1,
                Nombre = "Tenis",
                NumeroJugadores = 2
            };

            return new Pista
            {
                Id = 1,
                Nombre = "Pista 1",
                IdDeporte = 1,
                DeporteNavigation = itemDeporte1,
                Precio = 2
            };
        }

        private Reserva GetItemReserva()
        {
            Usuario itemUsuario1 = new Usuario
            {
                Id = 1,
                Nombre = "David",
                Password = "test1",
                Email = "email1@email.com",
                Telefono = "+34968555555",
                Borrado = false
            };

            Socio itemSocio1 = new Socio
            {
                Id = 1,
                Borrado = false,
                CodigoPostal = "30110",
                Direccion = "Dirreccion 1",
                Email = "test@email.com",
                IdUsuario = 1,
                Nombre = "David",
                Poblacion = "Población 1",
                Telefono = "968777777",
                UsuarioNavigation = itemUsuario1
            };

            Reserva itemReserva = new Reserva
            {
                Id = 1,
                IdUsuario = 1,
                Fecha = new DateTime(2019, 03, 10, 9, 0, 0),
                IdPista = 1,
                IdSocio = 1,
                Observaciones = "Test",
                PistaNavigation = GetItemPista(),
                SocioNavigation = itemSocio1,
                UsuarioNavigation = itemUsuario1
            };

            return itemReserva;
        }

        private List<Reserva> GetReservasTestDb()
        {
            //Usuarios
            Usuario itemUsuario1 = new Usuario
            {
                Id = 1,
                Nombre = "David",
                Password = "test1",
                Email = "email1@email.com",
                Telefono = "+34968555555",
                Borrado = false
            };
            Usuario itemUsuario2 = new Usuario
            {
                Id = 2,
                Nombre = "Pepe",
                Password = "test2",
                Email = "email2@email.com",
                Telefono = "+34968777777",
                Borrado = false
            };

            //Deportes
            Deportes itemDeporte1 = new Deportes
            {
                Id = 1,
                Nombre = "Tenis",
                NumeroJugadores = 2
            };

            //Pistas
            Pista itemPista1 = new Pista
            {
                Id = 1,
                Nombre = "Pista 1",
                IdDeporte = 1,
                DeporteNavigation = itemDeporte1,
                Precio = 2
            };

            Pista itemPista2 = new Pista
            {
                Id = 2,
                Nombre = "Pista 2",
                IdDeporte = 1,
                DeporteNavigation = itemDeporte1,
                Precio = 2
            };

            //Socio
            Socio itemSocio1 = new Socio
            {
                Id = 1,
                Borrado = false,
                CodigoPostal = "30110",
                Direccion = "Dirreccion 1",
                Email = "test@email.com",
                IdUsuario = 1,
                Nombre = "David",
                Poblacion = "Población 1",
                Telefono = "968777777",
                UsuarioNavigation = itemUsuario1
            };
            Socio itemSocio2 = new Socio
            {
                Id = 2,
                Borrado = false,
                CodigoPostal = "30111",
                Direccion = "Dirreccion 2",
                Email = "test2@email.com",
                IdUsuario = 2,
                Nombre = "Pepe",
                Poblacion = "Población 2",
                Telefono = "968888888",
                UsuarioNavigation = itemUsuario2
            };
            List<Reserva> itemReservas = new List<Reserva>
            {
                new Reserva
                {
                    Id = 1,
                    IdUsuario = 1,
                    Fecha = new DateTime(2019, 03, 10, 9, 0, 0),
                    IdPista = 1,
                    IdSocio = 1,
                    Observaciones = "Test",
                    PistaNavigation = itemPista1,
                    SocioNavigation = itemSocio1,
                    UsuarioNavigation = itemUsuario1
                },
                new Reserva
                {
                    Id = 2,
                    IdUsuario = 2,
                    Fecha = new DateTime(2019, 03, 11, 10, 0, 0),
                    IdPista = 2,
                    IdSocio = 1,
                    Observaciones = "Test",
                    PistaNavigation = itemPista2,
                    SocioNavigation = itemSocio2,
                    UsuarioNavigation = itemUsuario2
                }
            };

            return itemReservas;
        }

        private IReservaServices InjecReservasService(ClubDeportivoContext context)
        {
            IReservaRepository repository = new ReservaRepository(context);
            IReservaServices service = new ReservaServices(repository, _mapper);
            return service;
        }

        #endregion
    }
}
