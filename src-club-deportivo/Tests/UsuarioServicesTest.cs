using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Database;
using Database.Model;
using Database.Repositories.DeportesRepository;
using Database.Repositories.UsuarioRepository;
using FluentAssertions;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Service.DtoModels.Deportes;
using Service.Infrastructure.Security.CipherServices;
using Service.Security.CipherServices.Config;
using Service.Services.DeportesServices;
using Service.Services.UsuarioServices;
using Tests.Confing;
using TestSupport.EfHelpers;
using Xunit;
using NSubstitute;
using Service.DtoModels.Usuario;

namespace Tests
{
    public class UsuarioServicesTest
    {
        private readonly IMapper _mapper = null;

        public UsuarioServicesTest()
        {
            _mapper = AutoMapperConfigTest.GetImapper();
        }

        [Fact]
        public async Task GetUsuario1_Returns1()
        {
            List<Usuario> listItems = GetUsuariosTestDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IUsuarioServices usuarioService = InjecUsuarioService(context);
                DtoUsuario items = await usuarioService.GetUsuario(1);

                items.Nombre.Should().Be("David");
            }
        }

        [Fact]
        public async Task GetUsuario_NoExisting_Returns0()
        {
            List<Usuario> listItems = GetUsuariosTestDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IUsuarioServices usuarioService = InjecUsuarioService(context);
                DtoUsuario item = await usuarioService.GetUsuario(5);

                item.Should().BeNull();
            }
        }

        [Fact]
        public async Task GetUsuarios_AllUsuarios_Returns3()
        {
            List<Usuario> listItems = GetUsuariosTestDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IUsuarioServices usuarioService = InjecUsuarioService(context);
                List<DtoUsuario> items = await usuarioService.GetUsuarios(null, null);

                items.Should().HaveCount(3);
            }
        }

        [Fact]
        public async Task AddUsuario_NewUsuario_Returns1Inserted()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                context.CreateEmptyViaWipe();

                IUsuarioServices usuarioServiceService = InjecUsuarioService(context);

                DtoUsuarioAdd item = new DtoUsuarioAdd { Nombre = "Pepe", Email = "testNew@email.com", Password = "test", Telefono = "+34965542315"};

                await usuarioServiceService.AddUsuario(item);

                context.Usuario.FirstOrDefault().Should().NotBeNull();
            }
        }

        [Fact]
        public async Task UpdateUsuario1_ReturnsNewNombre()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                List<Usuario> listItems = GetUsuariosTestDb();
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);

                IUsuarioServices usuariosService = InjecUsuarioService(context);

                listItems[0].Nombre = "Test";

                DtoUsuarioUpdate itemUpdated = _mapper.Map<DtoUsuarioUpdate>(listItems[0]);
                await usuariosService.UpdateUsuario(itemUpdated);

                context.Usuario.FirstOrDefault().Should().NotBeNull();
                context.Usuario.First().Nombre.Should().Be("Test");
            }
        }

        [Fact]
        public async Task DeleteUsuario1_ReturnsNull()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                List<Usuario> listItems = new List<Usuario> { new Usuario { Id = 1, Nombre = "David", Password = "test1", Email = "email1@email.com", Telefono = "+34968555555", Borrado = false } };
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);

                IUsuarioServices usuarioService = InjecUsuarioService(context);

                await usuarioService.DeleteUsuario(1);

                context.Usuario.FirstOrDefault().Should().BeNull();
            }
        }

        #region "M�todos comunes"

        private List<Usuario> GetUsuariosTestDb()
        {
            List<Usuario> items = new List<Usuario>
            {
                new Usuario{Id = 1, Nombre= "David", Password = "test1", Email = "email1@email.com", Telefono = "+34968555555", Borrado = false },
                new Usuario{Id = 2, Nombre= "Pepe", Password = "test2", Email = "email2@email.com", Telefono = "+34968777777", Borrado = false},
                new Usuario{Id = 3, Nombre= "Juan", Password = "test3", Email = "email3@email.com", Telefono = "+34968888888", Borrado = false}
            };

            return items;
        }

        private IUsuarioServices InjecUsuarioService(ClubDeportivoContext context)
        {
            
            ICipherServices cipherServices = Substitute.For<ICipherServices>();
            IUsuarioRepository repository = new UsuarioRepository(context);
            IUsuarioServices service = new UsuarioServices(repository, cipherServices, _mapper);
            return service;
        }

        #endregion
    }
}
