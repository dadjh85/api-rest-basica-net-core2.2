using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Database;
using Database.Model;
using Database.Repositories.DeportesRepository;
using Database.Repositories.PistaRepository;
using Database.Repositories.ReservaRepository;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Service.DtoModels.Deportes;
using Service.DtoModels.Pista;
using Service.Services.DeportesServices;
using Service.Services.PistaServices;
using Service.Services.ReservaServices;
using Tests.Confing;
using TestSupport.EfHelpers;
using Xunit;

namespace Tests
{
    public class PistaServicesTest
    {
        private readonly IMapper _mapper = null;

        public PistaServicesTest()
        {
            _mapper = AutoMapperConfigTest.GetImapper();
        }

        [Fact]
        public async Task GetPista_Returns1()
        {
            List<Pista> listItems = GetPistaTesDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IPistaServices pistaService = InjecPistaService(context);
                DtoPista items = await pistaService.GetPista(1);

                items.Nombre.Should().Be("Pista 1");
            }
        }

        [Fact]
        public async Task GetPista_NoExisting_Returns0()
        {
            List<Pista> listItems = GetPistaTesDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IPistaServices pistaService = InjecPistaService(context);
                DtoPista item = await pistaService.GetPista(5);

                item.Should().BeNull();
            }
        }

        [Fact]
        public async Task GetPista_AllPistas_Returns4()
        {
            List<Pista> listItems = GetPistaTesDb();
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (var context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);
                IPistaServices pistaService = InjecPistaService(context);
                List<DtoPista> items = await pistaService.GetPistas();

                items.Should().HaveCount(4);
            }
        }

        [Fact]
        public async Task AddPista_NewPista_Returns1Inserted()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                await ContextConfig.SeedRangeDatabaseSameContext(context, new List<Deportes> { new Deportes { Id = 1, Nombre = "Tenis", NumeroJugadores = 2 } });

                IPistaServices pistaService = InjecPistaService(context);

                DtoPistaAdd item = new DtoPistaAdd { Nombre = "Pista 5", IdDeporte = 1, Precio = 2 };

                await pistaService.AddPista(item);

                context.Pista.FirstOrDefault().Should().NotBeNull();
            }
        }

        [Fact]
        public async Task UpdatePista1_ReturnsNewNombre()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                List<Pista> listItems = GetPistaTesDb();
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);

                IPistaServices pistaService = InjecPistaService(context);

                listItems[0].Nombre = "Test";

                DtoPistaUpdate itemUpdated = _mapper.Map<DtoPistaUpdate>(listItems[0]);
                await pistaService.UpdatePista(itemUpdated);

                context.Pista.FirstOrDefault().Should().NotBeNull();
                context.Pista.First().Nombre.Should().Be("Test");
            }
        }

        [Fact]
        public async Task DeletePista1_ReturnsNull()
        {
            DbContextOptions<ClubDeportivoContext> options = SqliteInMemory.CreateOptions<ClubDeportivoContext>();

            using (ClubDeportivoContext context = new ClubDeportivoContext(options))
            {
                Deportes itemDeporte1 = new Deportes { Id = 1, Nombre = "Tenis", NumeroJugadores = 2 };
                List<Pista> listItems = new List<Pista> { new Pista { Id = 1, Nombre = "Pista 1", IdDeporte = 1, DeporteNavigation = itemDeporte1, Precio = 2 } };
                await ContextConfig.SeedRangeDatabaseSameContext(context, listItems);

                IPistaServices pistaService = InjecPistaService(context);

                await pistaService.DeletePista(1);

                context.Pista.FirstOrDefault().Should().BeNull();
            }
        }

        #region "M�todos comunes"

        private List<Pista> GetPistaTesDb()
        {
            Deportes itemDeporte1 = new Deportes {Id = 1, Nombre = "Tenis", NumeroJugadores = 2};
            Deportes itemDeporte2 = new Deportes { Id = 2, Nombre = "F�tbol", NumeroJugadores = 22 };
            List<Pista> itemPistas = new List<Pista>
            {
                new Pista {Id = 1, Nombre= "Pista 1", IdDeporte = 1, DeporteNavigation = itemDeporte1, Precio = 2 },
                new Pista {Id = 2, Nombre= "Pista 2", IdDeporte = 1, DeporteNavigation = itemDeporte1, Precio = 3 },
                new Pista {Id = 3, Nombre= "Pista 3", IdDeporte = 2, DeporteNavigation = itemDeporte2, Precio = 1 },
                new Pista {Id = 4, Nombre= "Pista 4", IdDeporte = 2, DeporteNavigation = itemDeporte2, Precio = 4 },
            };

            return itemPistas;
        }

        private IPistaServices InjecPistaService(ClubDeportivoContext context)
        {
            IReservaServices reservaServices = Substitute.For<IReservaServices>();
            IPistaRepository repository = new PistaRepository(context);
            IPistaServices service = new PistaServices(repository, reservaServices, _mapper);
            return service;
        }

        #endregion
    }
}
