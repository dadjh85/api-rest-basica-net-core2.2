﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestSupport.EfHelpers;

namespace Tests.Confing
{
    public static class ContextConfig
    {
        public static async Task SeedRangeDatabaseSameContext<TContext, TEntity>(TContext seedContext, List<TEntity> entidades)
            where TContext : DbContext
            where TEntity : class
        {
            seedContext.CreateEmptyViaWipe();
            await seedContext.Set<TEntity>().AddRangeAsync(entidades);
            await seedContext.SaveChangesAsync();
        }
    }
}