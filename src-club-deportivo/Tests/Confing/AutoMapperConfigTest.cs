﻿using AutoMapper;
using Club.Api.Config;

namespace Tests.Confing
{
    public static class AutoMapperConfigTest
    {
        public static IMapper GetImapper()
        {
            AutoMapperConfig myProfile = new AutoMapperConfig();
            MapperConfiguration configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            return new Mapper(configuration);
        }
    }
}