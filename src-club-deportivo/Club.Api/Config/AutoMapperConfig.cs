﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Database.Model;
using Service.DtoModels;
using Service.DtoModels.Deportes;
using Service.DtoModels.Pista;
using Service.DtoModels.Reserva;
using Service.DtoModels.Socio;
using Service.DtoModels.Usuario;

namespace Club.Api.Config
{
    /// <summary>
    /// Clase de configuración de Mapa de mapeos personalizados de AutoMapper
    /// </summary>
    public class AutoMapperConfig : Profile
    {
        /// <summary>
        /// Constructor de la clase de configuración de mapeos de AutoMapper
        /// </summary>
        public AutoMapperConfig()
        {
            CreateMap<DtoDeportesEdit, Deportes>();
            CreateMap<Deportes, DtoDeportesEdit>();
            CreateMap<DtoPistaAdd, Pista>();
            CreateMap<Pista, DtoPistaAdd>();
            CreateMap<DtoSocioAdd, Socio>();
            CreateMap<Socio, DtoSocioAdd>();
            CreateMap<DtoReservaAdd, Reserva>();
            CreateMap<Reserva, DtoReservaAdd>();
            CreateMap<DtoUsuarioAdd, Usuario>();
            CreateMap<Usuario, DtoUsuarioAdd>();
        }
    }
}
