﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Club.Api.Config
{
    /// <summary>
    /// Clase de configuración de Swagger
    /// </summary>
    public static class SwaggerConfig
    {
        /// <summary>
        /// Registro de swagger que configura los servicios
        /// </summary>
        /// <param name="services"></param>
        /// <param name="env"></param>
        /// <returns></returns>
        public static IServiceCollection AddSwaggerRegistration(this IServiceCollection services, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                string xmlPath = Path.Combine(AppContext.BaseDirectory, "Club.Api.xml");
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1",
                        new Info
                        {
                            Title = "Api-Rest Net Core Club Deportivo",
                            Version = "v1"
                        });
                    c.IncludeXmlComments(xmlPath);

                    // Swagger 2.+ support
                    Dictionary<string, IEnumerable<string>> security = new Dictionary<string, IEnumerable<string>>
                    {
                        {"Bearer", new string[] { }},
                    };

                    c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                    {
                        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                        Name = "Authorization",
                        In = "header",
                        Type = "apiKey"
                    });

                    c.AddSecurityRequirement(security);
                });
            }
            return services;
        }

        /// <summary>
        /// Registro de swagger que configura la aplicación
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseSwagger(this IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                    {
                        c.SwaggerEndpoint($"/swagger/v1/swagger.json", "Api-Rest Net Core Club Deportivo");
                        c.RoutePrefix = string.Empty;
                    });
            }
            return app;
        }
    }
}
