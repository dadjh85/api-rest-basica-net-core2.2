﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Club.Api.Config
{
    /// <summary>
    /// Clase de configuración de contextos de base de datos en la API
    /// </summary>
    public static class DataContextConfig
    {
        /// <summary>
        /// Configuración de contexto ClubDeportivoContext
        /// </summary>
        /// <param name="services"></param>
        /// <param name="env"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddDbContextClubDeportivo(this IServiceCollection services, IConfiguration configuration, IHostingEnvironment env)
        {
            string connectionString = configuration.GetConnectionString("Default");
            services.AddDbContext<ClubDeportivoContext>(options => options.UseSqlServer(connectionString));
            return services;
        }
    }
}
