﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Club.Api.Config
{
    /// <summary>
    /// Clase de configuración de servicios de MvcCore
    /// </summary>
    public static class MvcCoreConfig
    {
        /// <summary>
        /// Método que registra la configuración que necesitamos de MvcCore
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddConfigMvcCore(this IServiceCollection services)
        {
            services.AddMvcCore(c =>
                {
                    AuthorizationPolicy policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser()
                        .Build();
                    c.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddAuthorization(m =>
                {
                    m.AddPolicy("CanReadSports", policyBuilder => policyBuilder.RequireClaim("CanReadSports", "true"));
                })
                .AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore)
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonFormatters()
                .AddApiExplorer()
                .AddDataAnnotations();

            return services;
        }
    }
}
