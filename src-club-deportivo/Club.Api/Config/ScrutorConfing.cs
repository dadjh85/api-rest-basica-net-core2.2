﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Club.Api.Config
{
    /// <summary>
    /// Clase de configuración de Scrutor
    /// Que permite inyectar todos los servicios y repositorios a la API
    /// </summary>
    public static class ScrutorConfing
    {
        /// <summary>
        /// Inyección de scrutor a los servicios de configuración
        /// </summary>
        /// <param name="services"></param>
        /// <param name="env"></param>
        /// <returns></returns>
        public static IServiceCollection AddScrutor(this IServiceCollection services, IHostingEnvironment env)
        {
            services.Scan(s => s.FromAssemblies(Assembly.Load("Service"), Assembly.Load("Database"))
                                .AddClasses(c => c.Where(e => e.Name.EndsWith("Services") || e.Name.EndsWith("Repository")))
                                .AsImplementedInterfaces()
                                .WithScopedLifetime());

            return services;
        }
    }
}
