﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Club.Api.Config;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Service.Security.CipherServices.Config;

namespace Club.Api
{
    /// <summary>
    /// Configuración de la API
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Inicio de la aplicación
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="environment"></param>
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        /// <summary>
        /// Configuración de la api
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Entorno de la api
        /// </summary>
        public IHostingEnvironment Environment { get; }

        /// <summary>
        /// Configuración de servicios
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            //Regostro de contexto ClubDeportivoContext
            services.AddDbContextClubDeportivo(Configuration, Environment);

            //Registro de AutoMapper
            services.AddAutoMapper(typeof(AutoMapperConfig).GetTypeInfo().Assembly);

            //Registro de los servicios de swagger
            services.AddSwaggerRegistration(Environment);

            //Registro de Scrutor
            services.AddScrutor(Environment);

            //Registro de autorización de la API
            services.AddAuthorization(configuration: Configuration);

            //Registro de configuración de MvcCore
            services.AddConfigMvcCore();

            services.Configure<CipherConfig>(Configuration.GetSection("Security"));
            services.AddSingleton(resolver => resolver.GetRequiredService<IOptions<CipherConfig>>().Value);
        }

        /// <summary>
        /// Configuración de la aplicación
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            //Registro de swagger en la aplicación
            app.UseAuthentication(); 
            app.UseSwagger(Environment);
            app.UseMvc();
        }
    }
}
