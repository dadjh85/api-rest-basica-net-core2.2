﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.DtoModels.Socio;
using Service.Services.SocioServices;

namespace Club.Api.Controllers
{
    /// <summary>
    /// Controlador para el mantenimiento de Socios
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class SociosController : Controller
    {
        #region "Propiedades y Constructores"

        private readonly ISocioServices _socioServices = null;

        /// <summary>
        /// Constructor del controaldor de Socio donde se le inyecta el servicio su servicio correspondiente.
        /// </summary>
        /// <param name="socioServices"></param>
        public SociosController(ISocioServices socioServices)
        {
            _socioServices = socioServices ?? throw new ArgumentNullException(nameof(socioServices));
        }

        #endregion

        #region "Implementación de acciones"

        /// <summary>
        /// Obtiene un listado de Socios
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<DtoSocio>>> GetSocios(int? page, int? pageSize)
        {
            List<DtoSocio> result = await _socioServices.GetSocios(page, pageSize);
            return Ok(result);
        }

        /// <summary>
        /// Obtiene un Socio por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<DtoSocio>> GetSocio(int id)
        {
            DtoSocio result = await _socioServices.GetSocio(id: id);
            return Ok(result);
        }

        /// <summary>
        /// Añade un Nuevo Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoSocioAdd>> AddSocio(DtoSocioAdd item)
        {
            DtoSocioAdd result = await _socioServices.AddSocio(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Actualiza un Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoSocio>> UpdateSocio(DtoSocioUpdate item)
        {
            DtoSocio result = await _socioServices.UpdateSocio(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        /// <summary>
        /// Hace un borrado lógico de un Socio
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> DeleteSocio(int id)
        {
            bool result = await _socioServices.DeleteSocio(id: id);

            if (!result)
            {
                return BadRequest();
            }

            return Ok();
        }

        #endregion
    }
}