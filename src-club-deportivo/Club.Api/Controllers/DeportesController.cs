﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.DtoModels;
using Service.Services.DeportesServices;
using Microsoft.AspNetCore.Http;
using Service.DtoModels.Deportes;
using Microsoft.AspNetCore.Authorization;

namespace Club.Api.Controllers
{
    /// <summary>
    /// Controlador para el mantenimiento de Deportes
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class DeportesController : ControllerBase
    {
        #region "Propiedades y Constructores"

        private readonly IDeportesServices _deportesServices = null;

        /// <summary>
        /// Constructor del controaldor de Deportes donde se le inyecta el servicio su servicio correspondiente.
        /// </summary>
        /// <param name="deportesServices"></param>
        public DeportesController(IDeportesServices deportesServices)
        {
            _deportesServices = deportesServices ?? throw new ArgumentNullException(nameof(deportesServices));
        }

        #endregion

        #region "Implementación de acciones"

        /// <summary>
        /// Obtiene el listado de todos los Deportes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize("CanReadSports")]
        public async Task<ActionResult<List<DtoDeportes>>> GetDeportes(int? page, int? pageSize)
        {
            List<DtoDeportes> result = await _deportesServices.GetDeportes(page, pageSize);
            return Ok(result);
        }

        /// <summary>
        /// Obtiene un Deporte por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<DtoDeportes>> GetDeporte(int id)
        {
            DtoDeportes result = await _deportesServices.GetDeporte(id: id);
            return Ok(result);
        }

        /// <summary>
        /// Añade un Deporte
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoDeportesEdit>> AddDeporte(DtoDeportesEdit item)
        {
            DtoDeportesEdit result = await _deportesServices.AddDeporte(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Actualiza un Deporte
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoDeportes>> UpdateDeporte(DtoDeportes item)
        {
            DtoDeportes result = await _deportesServices.UpdateDeporte(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        /// <summary>
        /// Elimina un Deporte
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> DeleteDeporte(int id)
        {
            bool result = await _deportesServices.DeleteDeporte(id: id);

            if (!result)
            {
                return BadRequest();
            }

            return Ok();
        }

        #endregion
    }
}