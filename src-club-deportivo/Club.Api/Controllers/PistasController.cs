﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.DtoModels.Pista;
using Service.Services.PistaServices;
using Microsoft.AspNetCore.Http;

namespace Club.Api.Controllers
{
    /// <summary>
    /// Controlador de Mantenimiento de Pistas
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class PistasController : Controller
    {
        #region "Propiedades y Constructores"

        private readonly IPistaServices _pistaServices = null;

        /// <summary>
        /// Constructor del controaldor de Pista donde se le inyecta el servicio su servicio correspondiente.
        /// </summary>
        /// <param name="pistaServices"></param>
        public PistasController(IPistaServices pistaServices)
        {
            _pistaServices = pistaServices ?? throw new ArgumentNullException(nameof(pistaServices));
        }

        #endregion

        #region "Implementación de acciones"

        /// <summary>
        /// Obtiene un listado de Pistas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<DtoPista>>> GetPistas(int? page, int? pageSize)
        {
            List<DtoPista> result = await _pistaServices.GetPistas(page, pageSize);
            return Ok(result);
        }
        
        /// <summary>
        /// Obtiene una pista por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<DtoPista>> GetPista(int id)
        {
            DtoPista result = await _pistaServices.GetPista(id: id);
            return Ok(result);
        }

        /// <summary>
        /// Añade una Pista nueva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoPistaAdd>> AddPista(DtoPistaAdd item)
        {
            DtoPistaAdd result = await _pistaServices.AddPista(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Actualiza una Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoPista>> UpdatePista(DtoPistaUpdate item)
        {
            DtoPista result = await _pistaServices.UpdatePista(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        /// <summary>
        /// Borra una Pista
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> DeletePista(int id)
        {
            bool result = await _pistaServices.DeletePista(id: id);

            if (!result)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Obtiene un listado de pistas disponibles verificando la fecha/hora,
        /// idUsuario y idDeporte
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idUsuario"></param>
        /// <param name="idDeporte"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("disponibles/{fecha}/{idUsuario}/{idDeporte}")]
        public async Task<ActionResult<List<DtoPista>>> GetPistasDisponibles(DateTime fecha, 
                                                                             int idUsuario,
                                                                             int idDeporte,
                                                                             int? page,
                                                                             int? pageSize)
        {
            List<DtoPista> result = await _pistaServices.GetPistaDisponibles(fecha: fecha,  
                                                                             idDeporte: idDeporte, 
                                                                             idUsuario:idUsuario,
                                                                             page: page,
                                                                             pageSize: pageSize);
            return Ok(result);
        }

        #endregion
    }
}