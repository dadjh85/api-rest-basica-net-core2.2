﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.DtoModels.Usuario;
using Service.Services.UsuarioServices;

namespace Club.Api.Controllers
{
    /// <summary>
    /// Controlador de mantenimiento de Usuarios
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class UsuarioController : Controller
    {
        #region "Propiedades y Constructores"

        private readonly IUsuarioServices _usuarioServices = null;

        /// <summary>
        /// Constructor del controaldor de Usuario donde se le inyecta el servicio su servicio correspondiente.
        /// </summary>
        /// <param name="usuarioServices"></param>
        public UsuarioController(IUsuarioServices usuarioServices)
        {
            _usuarioServices = usuarioServices ?? throw new ArgumentNullException(nameof(usuarioServices));
        }

        #endregion

        #region "Implementación de acciones"

        /// <summary>
        /// Obtiene un listado de Usuarios
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<DtoUsuario>>> GetUsuarios(int? page, int? pageSize)
        {
            List<DtoUsuario> result = await _usuarioServices.GetUsuarios(page, pageSize);
            return Ok(result);
        }

        /// <summary>
        /// Obtiene un Usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<DtoUsuario>> GetUsuario(int id)
        {
            DtoUsuario result = await _usuarioServices.GetUsuario(id: id);
            return Ok(result);
        }

        /// <summary>
        /// Añade un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoUsuarioAdd>> AddUsuario(DtoUsuarioAdd item)
        {
            DtoUsuarioAdd result = await _usuarioServices.AddUsuario(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Actualiza un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoUsuario>> UpdatePista(DtoUsuarioUpdate item)
        {
            DtoUsuario result = await _usuarioServices.UpdateUsuario(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        /// <summary>
        /// Hace un borrado lógico de un Usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> DeleteUsuario(int id)
        {
            bool result = await _usuarioServices.DeleteUsuario(id: id);

            if (!result)
            {
                return BadRequest();
            }

            return Ok();
        }

        #endregion
    }
}