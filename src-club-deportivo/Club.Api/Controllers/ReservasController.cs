﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.DtoModels.Reserva;
using Service.Services.ReservaServices;

namespace Club.Api.Controllers
{
    /// <summary>
    /// Controlador de mantenimiento de Reservas
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ReservasController : Controller
    {
        #region "Propiedades y Constructores"

        private readonly IReservaServices _reservaServices = null;

        /// <summary>
        /// Constructor del controaldor de Reserva donde se le inyecta el servicio su servicio correspondiente.
        /// </summary>
        /// <param name="reservaServices"></param>
        public ReservasController (IReservaServices reservaServices)
        {
            _reservaServices = reservaServices ?? throw new ArgumentNullException(nameof(reservaServices));
        }

        #endregion

        #region "Implementación de acciones"

        /// <summary>
        /// Obtiene un listado de Reservas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<DtoReserva>>> GetReservas(int? page, int? pageSize)
        {
            List<DtoReserva> result = await _reservaServices.GetReservas(page, pageSize);
            return Ok(result);
        }

        /// <summary>
        /// Obtiene una Reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<DtoReserva>> GetReserva(int id)
        {
            DtoReserva result = await _reservaServices.GetReserva(id: id);
            return Ok(result);
        }

        /// <summary>
        /// Añade un Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoReservaAdd>> AddReserva(DtoReservaAdd item)
        {
            DtoReservaAdd result = await _reservaServices.AddReserva(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Actualiza una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<DtoReserva>> UpdateReserva(DtoReservaUpdate item)
        {
            DtoReserva result = await _reservaServices.UpdateReserva(item: item);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        /// <summary>
        /// Borra una Reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> DeleteReserva(int id)
        {
            bool result = await _reservaServices.DeleteReserva(id: id);

            if (!result)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Obtiene un listado de reservar por día
        /// </summary>
        /// <returns></returns>
        [HttpGet("Dia/{fecha}")]
        public async Task<ActionResult<List<DtoReserva>>> GetReservasDia(DateTime fecha)
        {
            List<DtoReserva> result = await _reservaServices.GetReservasDia(fecha: fecha);
            return Ok(result);
        }

        #endregion
    }
}