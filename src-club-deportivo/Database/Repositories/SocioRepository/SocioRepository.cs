﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories.SocioRepository
{
    public class SocioRepository : ISocioRepository
    {
        #region "Propiedades y constructores"

        private readonly ClubDeportivoContext _context;

        public SocioRepository(ClubDeportivoContext context)
        {
            this._context = context;
        }

        #endregion

        #region "Implementación de ISocioRepository"

        /// <summary>
        /// Obtiene un Socio mediante el Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IQueryable<Socio> GetSocio(int id) => _context.Socio.Where(d => d.Id == id);

        /// <summary>
        /// Obtiene un listado de Socios
        /// </summary>
        /// <returns></returns>
        public IQueryable<Socio> GetSocios() => _context.Socio.AsNoTracking();

        /// <summary>
        /// Añade un Socio nuevo
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<Socio> AddSocio(Socio item)
        {
            if (item != null)
            {
                await _context.AddAsync(item);
                await _context.SaveChangesAsync();
                return item;
            }

            return null;
        }

        /// <summary>
        /// Actualiza un Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task UpdateSocio(Socio item)
        {
            if (item != null)
            {
                _context.Socio.Update(item);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Borra un Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task DeleteSocio(Socio item)
        {
            if (item != null)
            {
                _context.Socio.Remove(item);
                await _context.SaveChangesAsync();
            }
        }

        #endregion
    }
}
