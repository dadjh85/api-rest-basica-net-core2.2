﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Model;

namespace Database.Repositories.SocioRepository
{
    public interface ISocioRepository
    {
        /// <summary>
        /// Obtiene un Socio
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IQueryable<Socio> GetSocio(int id);

        /// <summary>
        /// Obtiene un listado de Socios
        /// </summary>
        /// <returns></returns>
        IQueryable<Socio> GetSocios();

        /// <summary>
        /// Añade un Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<Socio> AddSocio(Socio item);

        /// <summary>
        /// Actualiza un Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task UpdateSocio(Socio item);

        /// <summary>
        /// Elimina un Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task DeleteSocio(Socio item);
    }
}
