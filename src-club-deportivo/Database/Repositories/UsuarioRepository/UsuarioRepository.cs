﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories.UsuarioRepository
{
    public class UsuarioRepository : IUsuarioRepository
    {
        #region "Propiedades y constructores"

        private readonly ClubDeportivoContext _context;

        public UsuarioRepository
(ClubDeportivoContext context)
        {
            this._context = context;
        }

        #endregion

        #region "Implementación de IUsuarioRepository"

        /// <summary>
        /// Obtiene un Usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IQueryable<Usuario> GetUsuario(int id) => _context.Usuario.Where(d => d.Id == id);

        /// <summary>
        /// Obtiene un listado de Usuarios
        /// </summary>
        /// <returns></returns>
        public IQueryable<Usuario> GetUsuarios() => _context.Usuario.AsNoTracking();

        /// <summary>
        /// Añade un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<Usuario> AddUsuario(Usuario item)
        {
            if (item != null)
            {
                await _context.AddAsync(item);
                await _context.SaveChangesAsync();
                return item;
            }

            return null;
        }

        /// <summary>
        /// Actualiza un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task UpdateUsuario(Usuario item)
        {
            if (item != null)
            {
                _context.Usuario.Update(item);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Borra un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task DeleteUsuario(Usuario item)
        {
            if (item != null)
            {
                _context.Usuario.Remove(item);
                await _context.SaveChangesAsync();
            }
        }

        #endregion
    }
}
