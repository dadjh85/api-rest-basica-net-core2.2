﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Model;

namespace Database.Repositories.UsuarioRepository
{
    public interface IUsuarioRepository
    {
        /// <summary>
        /// Obtiene un Usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IQueryable<Usuario> GetUsuario(int id);

        /// <summary>
        /// Obtiene un listado de Usuarios
        /// </summary>
        /// <returns></returns>
        IQueryable<Usuario> GetUsuarios();

        /// <summary>
        /// Añade un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<Usuario> AddUsuario(Usuario item);

        /// <summary>
        /// Actualiza un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task UpdateUsuario(Usuario item);

        /// <summary>
        /// Borra un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task DeleteUsuario(Usuario item);
    }
}
