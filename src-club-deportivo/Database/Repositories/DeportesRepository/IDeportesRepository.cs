﻿using System.Linq;
using System.Threading.Tasks;
using Database.Model;

namespace Database.Repositories.DeportesRepository
{
    public interface IDeportesRepository
    {
        /// <summary>
        /// Obtiene un elemento del modelo Deportes
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IQueryable<Deportes> GetDeporte(int id);

        /// <summary>
        /// Obtiene todos los elementos de Deportes
        /// </summary>
        /// <returns></returns>
        IQueryable<Deportes> GetDeportes();


        /// <summary>
        /// Añade un elemento a Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<Deportes> AddDeporte(Deportes item);


        /// <summary>
        /// Actualiza un elemento de Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task UpdateDeporte(Deportes item);

        /// <summary>
        /// Borra un elemento de Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task DeleteDeporte(Deportes item);
    }
}
