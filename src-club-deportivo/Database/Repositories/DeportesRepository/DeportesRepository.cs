﻿using System.Linq;
using System.Threading.Tasks;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories.DeportesRepository
{
    public class DeportesRepository : IDeportesRepository
    {
        #region "Propiedades y constructores"

        private readonly ClubDeportivoContext _context;

        public DeportesRepository(ClubDeportivoContext context)
        {
            this._context = context;
        }

        #endregion

        #region "Implementación de IDeportesRepository"

        /// <summary>
        /// Obtiene un elemento del modelo Deportes
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IQueryable<Deportes> GetDeporte(int id) => _context.Deportes.Where(d => d.Id == id);

        /// <summary>
        /// Obtiene todos los elementos de Deportes
        /// </summary>
        /// <returns></returns>
        public IQueryable<Deportes> GetDeportes() => _context.Deportes.AsNoTracking();

        /// <summary>
        /// Añade un elemento a Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<Deportes> AddDeporte(Deportes item)
        {
            if (item != null)
            {
                await  _context.AddAsync(item);
                await _context.SaveChangesAsync();
                return item;
            }

            return null;

        }

        /// <summary>
        /// Actualiza un elemento de Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task UpdateDeporte(Deportes item)
        {
            if (item != null)
            {
                _context.Deportes.Update(item);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Borra un elemento de Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task DeleteDeporte(Deportes item)
        {
            if (item != null)
            {
                _context.Deportes.Remove(item);
                await _context.SaveChangesAsync();
            }
        }

        #endregion

    }
}
