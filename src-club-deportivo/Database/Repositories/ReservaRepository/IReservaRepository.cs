﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Model;

namespace Database.Repositories.ReservaRepository
{
    public interface IReservaRepository
    {
        /// <summary>
        /// Obtiene una Reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IQueryable<Reserva> GetReserva(int id);

        /// <summary>
        /// Obtiene un listado de Reservas
        /// </summary>
        /// <returns></returns>
        IQueryable<Reserva> GetReservas();

        /// <summary>
        /// Añade una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<Reserva> AddReserva(Reserva item);

        /// <summary>
        /// Actualiza una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task UpdateReserva(Reserva item);

        /// <summary>
        /// Borra una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task DeleteReserva(Reserva item);

        /// <summary>
        /// Obtiene las reservas confirmadas en la fecha seleccionada
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        IQueryable<Reserva> GetReservasDia(DateTime fecha);

        /// <summary>
        /// Obtiene las reservas de un usuario en un mismo día
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        IQueryable<Reserva> GetReservasUsuarioDia(DateTime fecha, int idUsuario);

        /// <summary>
        /// Obtiene las reservas de un usuario el mismo día y la misma hora
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        IQueryable<Reserva> GetReservasUsuarioMismaHora(DateTime fecha, int idUsuario);

        /// <summary>
        /// Obtiene el listado de reservas filtrando por día y hora
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idPista"></param>
        /// <returns></returns>
        IQueryable<Reserva> GetReservasPistaDiaHora(DateTime fecha, int idPista);
    }
}
