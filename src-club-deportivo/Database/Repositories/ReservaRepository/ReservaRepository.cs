﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories.ReservaRepository
{
    public class ReservaRepository : IReservaRepository
    {
        #region "Propiedades y constructores"

        private readonly ClubDeportivoContext _context;

        public ReservaRepository(ClubDeportivoContext context)
        {
            this._context = context;
        }

        #endregion

        #region "Implementación de IReservaRepository"

        /// <summary>
        /// Obtiene una Reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IQueryable<Reserva> GetReserva(int id) => _context.Reserva.Where(d => d.Id == id);

        /// <summary>
        /// Obtiene un listado de Reservas
        /// </summary>
        /// <returns></returns>
        public IQueryable<Reserva> GetReservas() => _context.Reserva.AsNoTracking();

        /// <summary>
        /// Añade una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<Reserva> AddReserva(Reserva item)
        {
            if (item != null)
            {
                await _context.AddAsync(item);
                await _context.SaveChangesAsync();
                return item;
            }

            return null;
        }

        /// <summary>
        /// Actualiza una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task UpdateReserva(Reserva item)
        {
            if (item != null)
            {
                _context.Reserva.Update(item);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Borra una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task DeleteReserva(Reserva item)
        {
            if (item != null)
            {
                _context.Reserva.Remove(item);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Obtiene las reservas 
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public IQueryable<Reserva> GetReservasDia(DateTime fecha)
        {
            IQueryable<Reserva> result = _context.Reserva.Where(r => r.Fecha.Date == fecha.Date);

            return result;
        }

        /// <summary>
        /// Obtiene las reservas de un usuario en un mismo día
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public IQueryable<Reserva> GetReservasUsuarioDia(DateTime fecha, int idUsuario)
        {
            IQueryable<Reserva> result = _context.Reserva.Where(r => r.Fecha.Date == fecha.Date && r.IdUsuario == idUsuario);

            return result;
        }

        /// <summary>
        /// Obtiene las reservas de un usuario el mismo día a la misma hora
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public IQueryable<Reserva> GetReservasUsuarioMismaHora(DateTime fecha, int idUsuario)
        {
            IQueryable<Reserva> result = _context.Reserva.Where(r => r.Fecha == fecha && r.IdUsuario == idUsuario);

            return result;
        }

        /// <summary>
        /// Obtiene un listado de reservas filtrando por día y hora
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idPista"></param>
        /// <returns></returns>
        public IQueryable<Reserva> GetReservasPistaDiaHora(DateTime fecha, int idPista)
        {
            IQueryable<Reserva> result = _context.Reserva.Where(r => r.Fecha == fecha && r.IdPista == idPista);

            return result;
        }

        #endregion
    }
}
