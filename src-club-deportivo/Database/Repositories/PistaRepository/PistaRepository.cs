﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories.PistaRepository
{
    public class PistaRepository : IPistaRepository
    {
        #region "Propiedades y constructores"

        private readonly ClubDeportivoContext _context;

        public PistaRepository(ClubDeportivoContext context)
        {
            this._context = context;
        }

        #endregion

        #region "Implementación de IPistaRepository"

        /// <summary>
        /// Obtiene una Pista
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IQueryable<Pista> GetPista(int id) => _context.Pista.Where(d => d.Id == id);


        /// <summary>
        /// Obtiene un listado de pistas
        /// </summary>
        /// <returns></returns>
        public IQueryable<Pista> GetPistas() => _context.Pista.AsNoTracking();

        /// <summary>
        /// Añade una Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<Pista> AddPista(Pista item)
        {
            if (item != null)
            {
                await _context.AddAsync(item);
                await _context.SaveChangesAsync();
                return item;
            }

            return null;
        }

        /// <summary>
        /// Actualiza una Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task UpdatePista(Pista item)
        {
            if (item != null)
            {
                _context.Pista.Update(item);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Borra una Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task DeletePista(Pista item)
        {
            if (item != null)
            {
                _context.Pista.Remove(item);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Obtiene las pistas disponibles teniendo en cuenta la fecha de la reserva,
        /// el deporte y el usuario que la reserva
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idDeporte"></param>
        /// <returns></returns>
        public IQueryable<Pista> GetPistaDisponibles(DateTime fecha, int idDeporte)
        {
            IQueryable<Pista> resultPistasDeporte = _context.Pista.Where(p => p.IdDeporte == idDeporte);

            IQueryable<Pista> result = (from p in resultPistasDeporte
                                        where p.Id != (from r in _context.Reserva
                                                       where r.Fecha == fecha && r.IdPista == p.Id
                                                       select r.IdPista).FirstOrDefault()
                                        select p);

            return result;
        }

        #endregion
    }
}
