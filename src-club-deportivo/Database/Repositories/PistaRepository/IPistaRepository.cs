﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Model;

namespace Database.Repositories.PistaRepository
{
    public interface IPistaRepository
    {
        /// <summary>
        /// Obtiene una pista
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IQueryable<Pista> GetPista(int id);

        /// <summary>
        /// Obtiene un listado de pistas
        /// </summary>
        /// <returns></returns>
        IQueryable<Pista> GetPistas();

        /// <summary>
        /// Añade una Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<Pista> AddPista(Pista item);

        /// <summary>
        /// Actualiza una Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task UpdatePista(Pista item);

        /// <summary>
        /// Borra una Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task DeletePista(Pista item);


        /// <summary>
        /// Obtiene las pistas disponibles 
        /// dependiendo de los siguientes parametros: fecha, idDeporte, idUsuario
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idDeporte"></param>
        /// <returns></returns>
        IQueryable<Pista> GetPistaDisponibles(DateTime fecha, int idDeporte);
    }
}
