﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configuration
{
    internal class SocioConfiguration : IEntityTypeConfiguration<Socio>
    {
        /// <summary>
        /// Configuración del modelo socio
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Socio> builder)
        {
            builder.HasIndex(e => e.IdUsuario)
                .IsUnique();

            builder.Property(e => e.CodigoPostal)
                .HasMaxLength(10)
                .IsUnicode(false);

            builder.Property(e => e.Direccion)
                .HasMaxLength(250)
                .IsUnicode(false);

            builder.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Nombre)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Poblacion)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Telefono)
                .HasMaxLength(20)
                .IsUnicode(false);

            builder.HasOne(d => d.UsuarioNavigation)
                   .WithOne(p => p.Socio)
                   .HasForeignKey<Socio>(d => d.IdUsuario)
                   .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasQueryFilter(c => !c.Borrado);

            //Nuevo















        }
    }
}
