﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configuration
{
    internal class ReservaConfiguration : IEntityTypeConfiguration<Reserva>
    {
        /// <summary>
        /// Configuración del modelo Reserva
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Reserva> builder)
        {
            builder.Property(e => e.Fecha).HasColumnType("smalldatetime");

            builder.Property(e => e.Observaciones)
                .HasMaxLength(500)
                .IsUnicode(false);

            builder.HasOne(d => d.PistaNavigation)
                .WithMany(p => p.Reserva)
                .HasForeignKey(d => d.IdPista)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Reserva_Pista");

            builder.HasOne(d => d.SocioNavigation)
                .WithMany(p => p.Reserva)
                .HasForeignKey(d => d.IdSocio)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Reserva_Socio");

            builder.HasOne(d => d.UsuarioNavigation)
                .WithMany(p => p.Reserva)
                .HasForeignKey(d => d.IdUsuario)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Reserva_Usuario");
        }
    }
}
