﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configuration
{
    internal class DeportesConfiguration : IEntityTypeConfiguration<Deportes>
    {
        /// <summary>
        /// Configuración del modelo Deportes
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Deportes> builder)
        {
            builder.Property(e => e.Nombre)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false);
        }
    }
}
