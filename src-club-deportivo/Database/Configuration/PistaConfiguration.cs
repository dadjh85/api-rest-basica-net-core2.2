﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configuration
{
    internal class PistaConfiguration : IEntityTypeConfiguration<Pista>
    {
        /// <summary>
        /// Configuración del modelo Pista
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Pista> builder)
        {
            builder.Property(e => e.Nombre)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false);

            builder.HasOne(d => d.DeporteNavigation)
                   .WithMany(p => p.Pista)
                   .HasForeignKey(d => d.IdDeporte)
                   .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Pista_Deportes");
        }
    }
}
