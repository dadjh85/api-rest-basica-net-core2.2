﻿using System;
using Database.Configuration;
using Database.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Database
{
    public partial class ClubDeportivoContext : DbContext
    {
        #region "Propiedades"

        public virtual DbSet<Deportes> Deportes { get; set; }
        public virtual DbSet<Pista> Pista { get; set; }
        public virtual DbSet<Reserva> Reserva { get; set; }
        public virtual DbSet<Socio> Socio { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }

        #endregion

        #region "Constructores"

        public ClubDeportivoContext()
        {
        }

        public ClubDeportivoContext(DbContextOptions<ClubDeportivoContext> options)
            : base(options)
        {
        }

        #endregion

        #region "Sobrecargas"

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DeportesConfiguration())
                        .ApplyConfiguration(new PistaConfiguration())
                        .ApplyConfiguration(new ReservaConfiguration())
                        .ApplyConfiguration(new SocioConfiguration())
                        .ApplyConfiguration(new UsuarioConfiguration());
        }

        #endregion
    }
}
