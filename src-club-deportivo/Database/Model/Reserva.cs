﻿using System;
using System.Collections.Generic;

namespace Database.Model
{
    public partial class Reserva
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public int IdPista { get; set; }
        public int IdSocio { get; set; }
        public int IdUsuario { get; set; }
        public string Observaciones { get; set; }

        public virtual Pista PistaNavigation { get; set; }
        public virtual Socio SocioNavigation { get; set; }
        public virtual Usuario UsuarioNavigation { get; set; }
    }
}
