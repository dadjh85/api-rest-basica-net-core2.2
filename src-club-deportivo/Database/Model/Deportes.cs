﻿using System;
using System.Collections.Generic;

namespace Database.Model
{
    public partial class Deportes
    {
        public Deportes()
        {
            Pista = new HashSet<Pista>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int NumeroJugadores { get; set; }

        public virtual ICollection<Pista> Pista { get; set; }
    }
}
