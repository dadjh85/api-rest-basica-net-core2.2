﻿using System;
using System.Collections.Generic;

namespace Database.Model
{
    public partial class Socio
    {
        public Socio()
        {
            Reserva = new HashSet<Reserva>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Poblacion { get; set; }
        public string CodigoPostal { get; set; }
        public bool Borrado { get; set; }
        public int IdUsuario { get; set; }

        public virtual Usuario UsuarioNavigation { get; set; }
        public virtual ICollection<Reserva> Reserva { get; set; }
    }
}
