﻿using System;
using System.Collections.Generic;

namespace Database.Model
{
    public partial class Usuario
    {
        public Usuario()
        {
            Reserva = new HashSet<Reserva>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public bool Borrado { get; set; }

        public virtual Socio Socio { get; set; }
        public virtual ICollection<Reserva> Reserva { get; set; }
    }
}
