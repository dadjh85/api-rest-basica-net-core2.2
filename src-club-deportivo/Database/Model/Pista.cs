﻿using System;
using System.Collections.Generic;

namespace Database.Model
{
    public partial class Pista
    {
        public Pista()
        {
            Reserva = new HashSet<Reserva>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdDeporte { get; set; }
        public double Precio { get; set; }

        public virtual Deportes DeporteNavigation { get; set; }
        public virtual ICollection<Reserva> Reserva { get; set; }
    }
}
