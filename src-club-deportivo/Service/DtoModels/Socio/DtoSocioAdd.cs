﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Service.DtoModels.Socio
{
    public class DtoSocioAdd
    {
        [Required]
        [MaxLength(50)]
        public string Nombre { get; set; }

        [Required]
        [MaxLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [MaxLength(20)]
        public string Telefono { get; set; }

        [MaxLength(250)]
        public string Direccion { get; set; }

        [MaxLength(50)]
        public string Poblacion { get; set; }

        [MaxLength(10)]
        public string CodigoPostal { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int IdUsuario { get; set; }
    }
}
