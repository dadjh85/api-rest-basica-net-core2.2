﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Service.DtoModels.Usuario;

namespace Service.DtoModels.Socio
{
    public class DtoSocio
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Nombre { get; set; }

        [Required]
        [MaxLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [MaxLength(20)]
        public string Telefono { get; set; }

        [MaxLength(250)]
        public string Direccion { get; set; }

        [MaxLength(50)]
        public string Poblacion { get; set; }

        [MaxLength(10)]
        public string CodigoPostal { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int IdUsuario { get; set; }

        public virtual DtoUsuario UsuarioNavigation { get; set; }
    }
}
