﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Service.DtoModels.Pista;
using Service.DtoModels.Socio;
using Service.DtoModels.Usuario;

namespace Service.DtoModels.Reserva
{
    public class DtoReserva
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public DateTime Fecha { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int IdPista { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int IdSocio { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int IdUsuario { get; set; }

        [MaxLength(500)]
        public string Observaciones { get; set; }

        public virtual DtoPista IdPistaNavigation { get; set; }

        public virtual DtoSocio IdSocioNavigation { get; set; }

        public virtual DtoUsuario IdUsuarioNavigation { get; set; }
    }
}
