﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Service.Infrastructure.CustomDataAnnotations;

namespace Service.DtoModels.Reserva
{
    public class DtoReservaAdd
    {
        [Required]
        [CurrentDateNow(ErrorMessage = "La fecha de la reserva debe de ser superior a la de hoy")]
        public DateTime Fecha { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int IdPista { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int IdSocio { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int IdUsuario { get; set; }

        [MaxLength(500)]
        public string Observaciones { get; set; }
    }
}
