﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Service.DtoModels.Pista
{
    public class DtoPistaUpdate
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Nombre { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int IdDeporte { get; set; }

        [Required]
        [Range(0.5, double.MaxValue, ErrorMessage = "El precio es inferior al importe minimo: 0.5")]
        public double Precio { get; set; }
    }
}
