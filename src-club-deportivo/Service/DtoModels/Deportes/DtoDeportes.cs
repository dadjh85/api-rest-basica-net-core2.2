﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Service.DtoModels.Deportes
{
    public class DtoDeportes
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Nombre { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int NumeroJugadores { get; set; }
    }
}
