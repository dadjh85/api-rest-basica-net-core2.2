﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Service.DtoModels.Deportes
{
    public class DtoDeportesEdit
    {
        [Required]
        [MaxLength(200)]
        public string Nombre { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int NumeroJugadores { get; set; }
    }
}
