﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Database.Model;
using Database.Repositories.PistaRepository;
using Database.Repositories.ReservaRepository;
using Microsoft.EntityFrameworkCore;
using Service.DtoModels.Pista;
using Service.Services.ReservaServices;

namespace Service.Services.PistaServices
{
    public class PistaServices : IPistaServices
    {
        #region "Propiedades y constructores"

        private readonly IPistaRepository _pistaRepository;
        private readonly IReservaServices _reservaServices;
        private readonly IMapper _mapperConfig;

        public PistaServices(IPistaRepository pistaRepository, IReservaServices reservaServices, IMapper mapperConfigurator)
        {
            _pistaRepository = pistaRepository ?? throw new ArgumentNullException(nameof(pistaRepository));
            _reservaServices = reservaServices ?? throw new ArgumentNullException(nameof(reservaServices));
            _mapperConfig = mapperConfigurator ?? throw new ArgumentNullException(nameof(mapperConfigurator));
        }

        #endregion

        #region "Implementación de IPistaService"

        /// <summary>
        /// Obtiene una Pista por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<DtoPista> GetPista(int id)
        {
            DtoPista result = await _pistaRepository.GetPista(id: id)
                                                    .ProjectTo<DtoPista>(_mapperConfig.ConfigurationProvider)
                                                    .FirstOrDefaultAsync();

            return result;
        }

        /// <summary>
        /// Obtiene un listado de Pistas
        /// </summary>
        /// <returns></returns>
        public async Task<List<DtoPista>> GetPistas(int? page = null, int? pageSize = null)
        {
            List<DtoPista> result = null;

            IQueryable<Pista> resultIQueryable = _pistaRepository.GetPistas();

            if (pageSize != null && page != null)
            {
                int? skiped = (page > 1) ? (page - 1) * pageSize : 0;
                result = await resultIQueryable.ProjectTo<DtoPista>(_mapperConfig.ConfigurationProvider)
                                               .Skip((int)skiped).Take((int)pageSize)
                                               .ToListAsync();
            }
            else
            {
                result = await resultIQueryable.ProjectTo<DtoPista>(_mapperConfig.ConfigurationProvider)
                                               .ToListAsync();
            }
                

            return result;
        }

        /// <summary>
        /// Añade una nueva Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoPistaAdd> AddPista(DtoPistaAdd item)
        {
            Pista result = await _pistaRepository.AddPista(item: _mapperConfig.Map<Pista>(item));

            return _mapperConfig.Map<DtoPistaAdd>(result);
        }

        /// <summary>
        /// Actualiza una Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoPista> UpdatePista(DtoPistaUpdate item)
        {
            Pista itemUpdate = await _pistaRepository.GetPista(id: item.Id)
                                                     .FirstOrDefaultAsync();

            DtoPista result = null;
            if (itemUpdate != null)
            {
                itemUpdate.Nombre = item.Nombre;
                itemUpdate.IdDeporte = item.IdDeporte;
                itemUpdate.Precio = item.Precio;
                await _pistaRepository.UpdatePista(item: itemUpdate);
            }

            result = await GetPista(id: item.Id);

            return result;
        }

        /// <summary>
        /// Borra una Pista
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeletePista(int id)
        {
            Pista item = await _pistaRepository.GetPista(id: id)
                                               .FirstOrDefaultAsync();
            if (item != null)
            {
                await _pistaRepository.DeletePista(item: item);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Obtiene un listado de pistas disponibles teniendo en cuenta la fecha de la reserva
        /// el id del deporte y el id del usuario
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idDeporte"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public async Task<List<DtoPista>> GetPistaDisponibles(DateTime fecha, int idDeporte, int idUsuario, int? pageSize = null, int? page = null)
        {
            int reservasUsuarioDia = await _reservaServices.GetReservasUsuarioDia(fecha, idUsuario);
            int reservasUsuarioMismaHora = await _reservaServices.GetReservasUsuarioMismaHora(fecha, idUsuario);

            if (!_reservaServices.IsFechaHoraCorrecta(fecha))
            {
                return new List<DtoPista>();
            }
            else if (reservasUsuarioMismaHora >= 2)
            {
                return new List<DtoPista>();
            }
            else if (reservasUsuarioDia >= 3)
            {
                return new List<DtoPista>();
            }

            List<DtoPista> result = null;

            IQueryable<Pista> resultIQueryable = _pistaRepository.GetPistaDisponibles(fecha, idDeporte);

            if (pageSize != null && page != null)
            {
                int? skiped = (page > 1) ? (page - 1) * pageSize : 0;

                result = await resultIQueryable.ProjectTo<DtoPista>(_mapperConfig.ConfigurationProvider)
                                               .Skip((int)skiped).Take((int)pageSize)
                                               .ToListAsync();
            }
            else
            {
                result = await resultIQueryable.ProjectTo<DtoPista>(_mapperConfig.ConfigurationProvider)
                                               .ToListAsync();
            }

            return result;
        }
        #endregion
    }
}
