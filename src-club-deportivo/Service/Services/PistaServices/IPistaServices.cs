﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.DtoModels.Pista;

namespace Service.Services.PistaServices
{
    public interface IPistaServices
    {
        /// <summary>
        /// Obtiene una Pista
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<DtoPista> GetPista(int id);

        /// <summary>
        /// Obtiene un listado de Pistas
        /// </summary>
        /// <returns></returns>
        Task<List<DtoPista>> GetPistas(int? page = null, int? pageSize = null);

        /// <summary>
        /// Añade una nueva Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoPistaAdd> AddPista(DtoPistaAdd item);

        /// <summary>
        /// Actualiza una Pista
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoPista> UpdatePista(DtoPistaUpdate item);

        /// <summary>
        /// Borra una Pista
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeletePista(int id);

        /// <summary>
        /// Obtiene las pistas disponibles teniendo en cuenta la fecha de la reserva,
        /// el idDeporte y el idUsuario
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idDeporte"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        Task<List<DtoPista>> GetPistaDisponibles(DateTime fecha, int idDeporte, int idUsuario, int? pageSize = null, int? page = null);
    }
}
