﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.DtoModels.Reserva;

namespace Service.Services.ReservaServices
{
    public interface IReservaServices
    {
        /// <summary>
        /// Obtiene una Reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<DtoReserva> GetReserva(int id);

        /// <summary>
        /// Obtiene un listado de Reservas
        /// </summary>
        /// <returns></returns>
        Task<List<DtoReserva>> GetReservas(int? page, int? pageSize);

        /// <summary>
        /// Añade una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoReservaAdd> AddReserva(DtoReservaAdd item);

        /// <summary>
        /// Actualiza una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoReserva> UpdateReserva(DtoReservaUpdate item);

        /// <summary>
        /// Borra una Reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteReserva(int id);

        /// <summary>
        /// Obtiene las reservas realizada en un día concreto
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        Task<List<DtoReserva>> GetReservasDia(DateTime fecha);

        /// <summary>
        /// Obtiene las reservas de un usuario en un mismo día
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        Task<int> GetReservasUsuarioDia(DateTime fecha, int idUsuario);
        
        /// <summary>
        /// Obtiene la cantidad de reservas de un usuario un mismo día a la misma hora
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        Task<int> GetReservasUsuarioMismaHora(DateTime fecha, int idUsuario);

        /// <summary>
        /// Verifica que la hora de la reserva sea correcta
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        bool IsFechaHoraCorrecta(DateTime fecha);

    }
}
