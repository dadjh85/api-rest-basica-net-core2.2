﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Database.Model;
using Database.Repositories.ReservaRepository;
using Microsoft.EntityFrameworkCore;
using Service.DtoModels.Pista;
using Service.DtoModels.Reserva;

namespace Service.Services.ReservaServices
{
    public class ReservaServices : IReservaServices
    {
        #region "Propiedades y constructores"

        private readonly IReservaRepository _reservaRepository;
        private readonly IMapper _mapperConfig;

        public ReservaServices(IReservaRepository reservaRepository, IMapper mapperConfigurator)
        {
            _reservaRepository = reservaRepository ?? throw new ArgumentNullException(nameof(reservaRepository));
            _mapperConfig = mapperConfigurator ?? throw new ArgumentNullException(nameof(mapperConfigurator));
        }

        #endregion

        #region "Implementación de IReservaServices"

        /// <summary>
        /// Obtiene una Reserva por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<DtoReserva> GetReserva(int id)
        {
            DtoReserva result = await _reservaRepository.GetReserva(id: id)
                                                        .ProjectTo<DtoReserva>(_mapperConfig.ConfigurationProvider)
                                                        .FirstOrDefaultAsync();

            return result;
        }

        /// <summary>
        /// Obtiene un listado de Reservas
        /// </summary>
        /// <returns></returns>
        public async Task<List<DtoReserva>> GetReservas(int? page, int? pageSize)
        {
            List<DtoReserva> result = null;

            IQueryable<Reserva> resultIQueryable =  _reservaRepository.GetReservas();

            if (pageSize != null && page != null)
            {
                int? skiped = (page > 1) ? (page - 1) * pageSize : 0;
                result = await resultIQueryable.ProjectTo<DtoReserva>(_mapperConfig.ConfigurationProvider)
                                               .Skip((int)skiped).Take((int)pageSize)
                                               .ToListAsync();
            }
            else
            {
                result = await resultIQueryable.ProjectTo<DtoReserva>(_mapperConfig.ConfigurationProvider)
                                               .ToListAsync();
            }
                

            return result;
        }

        /// <summary>
        /// Añade una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoReservaAdd> AddReserva(DtoReservaAdd item)
        {
            if (! await IsReservaCorrecta(item.Fecha, item.IdUsuario, item.IdPista))
                return null;

            Reserva result = await _reservaRepository.AddReserva(item: _mapperConfig.Map<Reserva>(item));

            return _mapperConfig.Map<DtoReservaAdd>(result);
        }

        /// <summary>
        /// Actualiza una Reserva
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoReserva> UpdateReserva(DtoReservaUpdate item)
        {
            Reserva itemUpdate = await _reservaRepository.GetReserva(id: item.Id)
                                                         .FirstOrDefaultAsync();

            DtoReserva result = null;
            if (itemUpdate != null)
            {
                bool changeFechaUsuario = item.Fecha != itemUpdate.Fecha || item.IdUsuario != itemUpdate.IdUsuario;
                bool changeFechaPista = item.Fecha != itemUpdate.Fecha || item.IdPista != itemUpdate.IdPista;
                if (!await IsReservaCorrecta(item.Fecha, item.IdUsuario, item.IdPista, changeFechaUsuario, changeFechaPista))
                    return null;

                itemUpdate.Fecha = item.Fecha;
                itemUpdate.IdPista = item.IdPista;
                itemUpdate.IdSocio = item.IdSocio;
                itemUpdate.IdUsuario = item.IdUsuario;
                itemUpdate.Observaciones = item.Observaciones;
                await _reservaRepository.UpdateReserva(item: itemUpdate);
            }

            result = await GetReserva(id: item.Id);

            return result;
        }

        /// <summary>
        /// Borra una Reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteReserva(int id)
        {
            Reserva item = await _reservaRepository.GetReserva(id: id)
                                                   .FirstOrDefaultAsync();
            if (item != null)
            {
                await _reservaRepository.DeleteReserva(item: item);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Obtiene un listado de reservas de un día concreto
        /// </summary>
        /// <returns></returns>
        public async Task<List<DtoReserva>> GetReservasDia(DateTime fecha)
        {
            List<DtoReserva> result = await _reservaRepository.GetReservasDia(fecha: fecha)
                                                              .ProjectTo<DtoReserva>(_mapperConfig.ConfigurationProvider)
                                                              .ToListAsync();

            return result;
        }

        /// <summary>
        /// Obtiene un listado de reservas de un usuario en un mismo día
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public async Task<int> GetReservasUsuarioDia(DateTime fecha, int idUsuario)
        {
            List<DtoReserva> result = await _reservaRepository.GetReservasUsuarioDia(fecha: fecha, idUsuario: idUsuario)
                                                               .ProjectTo<DtoReserva>(_mapperConfig.ConfigurationProvider)
                                                               .ToListAsync();

            return result.Count;
        }

        /// <summary>
        /// Obtiene las reservas de un usuario el mismo día a la misma hora
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public async Task<int> GetReservasUsuarioMismaHora(DateTime fecha, int idUsuario)
        {
            List<DtoReserva> result = await _reservaRepository.GetReservasUsuarioMismaHora(fecha: fecha, idUsuario: idUsuario)
                                                              .ProjectTo<DtoReserva>(_mapperConfig.ConfigurationProvider)
                                                              .ToListAsync();

            return result.Count;
        }

        public async Task<bool> IsReservaCorrecta(DateTime fecha, int idUsuario, int idPista, 
                                                  bool isChangeFechaUsuario = true, 
                                                  bool isChangeFechaPista = true)
        {
            int reservasUsuarioDia = await GetReservasUsuarioDia(fecha, idUsuario);
            int reservasUsuarioMismaHora = await GetReservasUsuarioMismaHora(fecha, idUsuario);
            List<DtoReserva> reservasPistaDiaHora = await _reservaRepository.GetReservasPistaDiaHora(fecha, idPista)
                                                                      .ProjectTo<DtoReserva>(_mapperConfig.ConfigurationProvider)
                                                                      .ToListAsync();

            if (!IsFechaHoraCorrecta(fecha))
            {
                return false;
            }
            if ((reservasUsuarioMismaHora >= 2 && isChangeFechaUsuario) || (reservasUsuarioMismaHora > 2 && !isChangeFechaUsuario))
            {
                return false;
            }
            if ((reservasUsuarioDia >= 3 && isChangeFechaUsuario) || (reservasUsuarioDia > 3 && !isChangeFechaUsuario))
            {
                return false;
            }
            if ((reservasPistaDiaHora.Count > 0 && isChangeFechaPista) || (reservasPistaDiaHora.Count > 1 && !isChangeFechaPista))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Comprueba que las hora estén en el rango correcto
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public bool IsFechaHoraCorrecta(DateTime fecha)
        {
            TimeSpan horaInicio = new TimeSpan(8, 0, 0);
            TimeSpan horaFin = new TimeSpan(22, 0, 0);

            TimeSpan horaComprobar = new TimeSpan(fecha.Hour, fecha.Minute, fecha.Second);

            return horaFin > horaComprobar && horaComprobar > horaInicio;
        }

        #endregion
    }
}
