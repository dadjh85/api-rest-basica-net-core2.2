﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.DtoModels.Usuario;

namespace Service.Services.UsuarioServices
{
    public interface IUsuarioServices
    {
        /// <summary>
        /// Obtiene un Usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<DtoUsuario> GetUsuario(int id);

        /// <summary>
        /// Obtiene un Listado de Usuarios
        /// </summary>
        /// <returns></returns>
        Task<List<DtoUsuario>> GetUsuarios(int? page, int? pageSize);

        /// <summary>
        /// Añade un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoUsuarioAdd> AddUsuario(DtoUsuarioAdd item);

        /// <summary>
        /// Actualiza un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoUsuario> UpdateUsuario(DtoUsuarioUpdate item);

        /// <summary>
        /// Hace un Borrado logico de un Usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteUsuario(int id);
    }
}
