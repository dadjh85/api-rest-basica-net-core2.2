﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Database.Model;
using Database.Repositories.UsuarioRepository;
using Microsoft.EntityFrameworkCore;
using Service.DtoModels.Usuario;
using System;
using Service.Infrastructure.Security.CipherServices;
using System.Linq;

namespace Service.Services.UsuarioServices
{
    public class UsuarioServices : IUsuarioServices
    {
        #region "Propiedades y constructores"

        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IMapper _mapperConfig;
        private readonly ICipherServices _cipherServices;

        public UsuarioServices(IUsuarioRepository usuarioRepository, ICipherServices cipherServices, IMapper mapperConfigurator)
        {
            _usuarioRepository = usuarioRepository ?? throw new ArgumentNullException(nameof(usuarioRepository));
            _cipherServices = cipherServices ?? throw new ArgumentNullException(nameof(cipherServices));
            _mapperConfig = mapperConfigurator ?? throw new ArgumentNullException(nameof(mapperConfigurator));
        }

        #endregion

        #region "Implementación de ISocioService"

        /// <summary>
        /// Obtiene un Usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<DtoUsuario> GetUsuario(int id)
        {
            DtoUsuario result = await _usuarioRepository.GetUsuario(id: id)
                                                        .ProjectTo<DtoUsuario>(_mapperConfig.ConfigurationProvider)
                                                        .FirstOrDefaultAsync();

            return result;
        }

        /// <summary>
        /// Obtiene un listado de Usuarios
        /// </summary>
        /// <returns></returns>
        public async Task<List<DtoUsuario>> GetUsuarios(int? page, int? pageSize)
        {
            List<DtoUsuario> result = null;

            IQueryable<Usuario> resultIQueryable = _usuarioRepository.GetUsuarios();

            if (pageSize != null && page != null)
            {
                int? skiped = (page > 1) ? (page - 1) * pageSize : 0;

                result = await resultIQueryable.ProjectTo<DtoUsuario>(_mapperConfig.ConfigurationProvider)
                                               .Skip((int)skiped).Take((int)pageSize)
                                               .ToListAsync();
            }
            else
            {
                result = await resultIQueryable.ProjectTo<DtoUsuario>(_mapperConfig.ConfigurationProvider)
                                               .ToListAsync();
            }

            return result;
        }

        /// <summary>
        /// Añade un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoUsuarioAdd> AddUsuario(DtoUsuarioAdd item)
        {
            Usuario itemInsert = _mapperConfig.Map<Usuario>(item);
            itemInsert.Password = _cipherServices.Encrypt(item.Password); 

            Usuario result = await _usuarioRepository.AddUsuario(item: itemInsert);

            return _mapperConfig.Map<DtoUsuarioAdd>(result); 
        }

        /// <summary>
        /// Actualiza un Usuario
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoUsuario> UpdateUsuario(DtoUsuarioUpdate item)
        {
            Usuario itemUpdate = await _usuarioRepository.GetUsuario(id: item.Id)
                                                         .FirstOrDefaultAsync();
            DtoUsuario result = null;
            if (itemUpdate != null)
            {
                itemUpdate.Nombre = item.Nombre;
                itemUpdate.Password = _cipherServices.Encrypt(item.Password);
                itemUpdate.Email = item.Email;
                itemUpdate.Telefono = item.Telefono;
                itemUpdate.Telefono = item.Telefono;
                await _usuarioRepository.UpdateUsuario(item: itemUpdate);
            }

            result = await GetUsuario(id: item.Id);

            return result;
        }

        /// <summary>
        /// Hace un borrado logico de un Usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteUsuario(int id)
        {
            Usuario item = await _usuarioRepository.GetUsuario(id: id)
                                                   .FirstOrDefaultAsync();
            if (item != null)
            {
                item.Borrado = true;
                await _usuarioRepository.UpdateUsuario(item: item);
                return true;
            }
            return false;
        }

        #endregion
    }
}
