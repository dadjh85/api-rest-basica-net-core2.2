﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Database.Model;
using Database.Repositories.DeportesRepository;
using Microsoft.EntityFrameworkCore;
using Service.DtoModels;
using Service.DtoModels.Deportes;

namespace Service.Services.DeportesServices
{
    public class DeportesServices : IDeportesServices
    {
        #region "Propiedades y constructores"

        private readonly IDeportesRepository _deportesRepository;
        private readonly IMapper _mapperConfig;

        public DeportesServices(IDeportesRepository deportesRepository, IMapper mapperConfigurator)
        {
            _deportesRepository = deportesRepository ?? throw new ArgumentNullException(nameof(deportesRepository));
            _mapperConfig = mapperConfigurator ?? throw new ArgumentNullException(nameof(mapperConfigurator));
        }

        #endregion

        #region "Implementación de IDeportesServices"

        /// <summary>
        /// Obtiene un elemento de Deportes
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<DtoDeportes> GetDeporte(int id)
        {
            DtoDeportes result = await _deportesRepository.GetDeporte(id: id)
                                                         .ProjectTo<DtoDeportes>(_mapperConfig.ConfigurationProvider)
                                                         .FirstOrDefaultAsync();

            return result;
        }

        /// <summary>
        /// Obtiene los elementos de Deportes
        /// </summary>
        /// <returns></returns>
        public async Task<List<DtoDeportes>> GetDeportes(int? page, int? pageSize)
        {
            List<DtoDeportes> result = null;

            IQueryable<Deportes> resultIQueryable = _deportesRepository.GetDeportes();
            if (pageSize != null && page != null)
            {
                int? skiped = (page > 1) ? (page - 1) * pageSize : 0;
                result = await resultIQueryable.ProjectTo<DtoDeportes>(_mapperConfig.ConfigurationProvider)
                                               .Skip((int)skiped).Take((int)pageSize)
                                               .ToListAsync();
            }
            else
            {
                result = await resultIQueryable.ProjectTo<DtoDeportes>(_mapperConfig.ConfigurationProvider)
                                               .ToListAsync();
            }
                

            return result;
        }

        /// <summary>
        /// Añade un elemento a Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoDeportesEdit> AddDeporte(DtoDeportesEdit item)
        {
            Deportes result = await _deportesRepository.AddDeporte(item: _mapperConfig.Map<Deportes>(item));

            return _mapperConfig.Map<DtoDeportesEdit>(result);
        }

        /// <summary>
        /// Actualiza un elemento de Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoDeportes> UpdateDeporte(DtoDeportes item)
        {
            Deportes itemUpdate = await _deportesRepository.GetDeporte(id: item.Id)
                                                           .FirstOrDefaultAsync();

            DtoDeportes result = null;
            if (itemUpdate != null)
            {
                itemUpdate.Nombre = item.Nombre;
                itemUpdate.NumeroJugadores = item.NumeroJugadores;
                await _deportesRepository.UpdateDeporte(item: itemUpdate);
            }

            result = await GetDeporte(id: item.Id);

            return result;
        }

        /// <summary>
        /// Borrar un elemento de Deporte
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteDeporte(int id)
        {
            Deportes item = await _deportesRepository.GetDeporte(id: id)
                                                    .FirstOrDefaultAsync();
            if (item != null)
            {
                await _deportesRepository.DeleteDeporte(item: item);
                return true;
            }

            return false;
        }

        #endregion
    }
}
