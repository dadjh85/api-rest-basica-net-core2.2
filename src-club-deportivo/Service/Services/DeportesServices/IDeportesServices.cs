﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.DtoModels;
using Service.DtoModels.Deportes;

namespace Service.Services.DeportesServices
{
    public interface IDeportesServices
    {
        /// <summary>
        /// Obtiene un elemento de Deportes
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<DtoDeportes> GetDeporte(int id);

        /// <summary>
        /// Obtiene los elementos de Deportes
        /// </summary>
        /// <returns></returns>
        Task<List<DtoDeportes>> GetDeportes(int? page, int? pageSize);

        /// <summary>
        /// Añade un elemento en Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoDeportesEdit> AddDeporte(DtoDeportesEdit item);

        /// <summary>
        /// Actualiza un elemento en Deportes
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoDeportes> UpdateDeporte(DtoDeportes item);

        /// <summary>
        /// Borra un elemento en Deportes
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteDeporte(int id);
    }
}
