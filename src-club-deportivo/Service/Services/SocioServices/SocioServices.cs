﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Database.Model;
using Database.Repositories.SocioRepository;
using Microsoft.EntityFrameworkCore;
using Service.DtoModels.Socio;

namespace Service.Services.SocioServices
{
    public class SocioServices : ISocioServices
    {
        #region "Propiedades y constructores"

        private readonly ISocioRepository _socioRepository;
        private readonly IMapper _mapperConfig;

        public SocioServices(ISocioRepository socioRepository, IMapper mapperConfigurator)
        {
            _socioRepository = socioRepository ?? throw new ArgumentNullException(nameof(socioRepository));
            _mapperConfig = mapperConfigurator ?? throw new ArgumentNullException(nameof(mapperConfigurator));
        }

        #endregion

        #region "Implementación de ISocioService"

        /// <summary>
        /// Obtiene un socio por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<DtoSocio> GetSocio(int id)
        {
            DtoSocio result = await _socioRepository.GetSocio(id: id)
                                                    .ProjectTo<DtoSocio>(_mapperConfig.ConfigurationProvider)
                                                    .FirstOrDefaultAsync();

            return result;
        }

        /// <summary>
        /// Obtiene un listado de socios
        /// </summary>
        /// <returns></returns>
        public async Task<List<DtoSocio>> GetSocios(int? page, int? pageSize)
        {
            List<DtoSocio> result = null;

            IQueryable<Socio> resultIQueryable = _socioRepository.GetSocios();
            if (pageSize != null && page != null)
            {
                int? skiped = (page > 1) ? (page - 1) * pageSize : 0;

                result = await resultIQueryable.ProjectTo<DtoSocio>(_mapperConfig.ConfigurationProvider)
                                               .Skip((int)skiped).Take((int)pageSize)
                                               .ToListAsync();
            }
            else
            {
                result = await resultIQueryable.ProjectTo<DtoSocio>(_mapperConfig.ConfigurationProvider)
                                               .ToListAsync();
            }
                
            return result;
        }

        /// <summary>
        /// Áñade un nuevo Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoSocioAdd> AddSocio(DtoSocioAdd item)
        {
            Socio result = await _socioRepository.AddSocio(item: _mapperConfig.Map<Socio>(item));

            return _mapperConfig.Map<DtoSocioAdd>(result);
        }

        /// <summary>
        /// Actualiza un Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<DtoSocio> UpdateSocio(DtoSocioUpdate item)
        {
            Socio itemUpdate = await _socioRepository.GetSocio(id: item.Id)
                                                     .FirstOrDefaultAsync();

            DtoSocio result = null;
            if (itemUpdate != null)
            {
                itemUpdate.Nombre = item.Nombre;
                itemUpdate.Email = item.Email;
                itemUpdate.Telefono = item.Telefono;
                itemUpdate.Direccion = item.Direccion;
                itemUpdate.Poblacion = item.Poblacion;
                itemUpdate.CodigoPostal = item.CodigoPostal;
                itemUpdate.IdUsuario = item.IdUsuario;
                await _socioRepository.UpdateSocio(item: itemUpdate);
            }

            result = await GetSocio(id: item.Id);

            return result;
        }

        /// <summary>
        /// Hace un Borrado logico de un Socio
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSocio(int id)
        {
            Socio item = await _socioRepository.GetSocio(id: id)
                                               .FirstOrDefaultAsync();
            if (item != null)
            {
                item.Borrado = true;
                await _socioRepository.UpdateSocio(item: item);
                return true;
            }
            return false;
        }

        #endregion
    }
}
