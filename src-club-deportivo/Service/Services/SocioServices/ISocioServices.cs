﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.DtoModels.Socio;

namespace Service.Services.SocioServices
{
    public interface ISocioServices
    {
        /// <summary>
        /// Obtiene un Socio
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<DtoSocio> GetSocio(int id);

        /// <summary>
        /// Obtiene un listado de Socios
        /// </summary>
        /// <returns></returns>
        Task<List<DtoSocio>> GetSocios(int? page, int? pageSize);

        /// <summary>
        /// Añade un nuevo Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoSocioAdd> AddSocio(DtoSocioAdd item);

        /// <summary>
        /// Actualiza un Socio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<DtoSocio> UpdateSocio(DtoSocioUpdate item);

        /// <summary>
        /// Borra un Socio
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteSocio(int id);
    }
}
