﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Infrastructure.Security
{
    /// <summary>
    /// Clase de configuración que mapea Opciones de appSetting
    /// </summary>
    public class CryptographyKeyOptions
    {
        /// <summary>
        /// Campo de configuración de criptografía
        /// </summary>
        public string Key { get; set; }
    }
}
