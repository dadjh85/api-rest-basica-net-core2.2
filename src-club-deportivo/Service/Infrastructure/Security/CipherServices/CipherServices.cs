﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Options;
using Service.Security.CipherServices;
using Service.Security.CipherServices.Config;

namespace Service.Infrastructure.Security.CipherServices
{
    public class CipherServices : ICipherServices
    {
        private readonly IDataProtectionProvider _dataProtectionProvider;
        private readonly CipherConfig _cipherConfig = null;

        public CipherServices(IDataProtectionProvider dataProtectionProvider, CipherConfig cipherConfig)
        {
            _dataProtectionProvider = dataProtectionProvider;
            _cipherConfig = cipherConfig;
        }

        public string Encrypt(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }

            IDataProtector protector = _dataProtectionProvider.CreateProtector(_cipherConfig.Key);
            return protector.Protect(input);
        }

        public string Decrypt(string cipherText)
        {
            if (string.IsNullOrEmpty(cipherText))
            {
                return "";
            }

            IDataProtector protector = _dataProtectionProvider.CreateProtector(_cipherConfig.Key);
            return protector.Unprotect(cipherText);
        }
    }
}
