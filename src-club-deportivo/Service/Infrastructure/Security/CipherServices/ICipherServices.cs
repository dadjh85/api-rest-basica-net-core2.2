﻿namespace Service.Infrastructure.Security.CipherServices
{
	public interface ICipherServices
	{
		string Decrypt(string cipherText);
		string Encrypt(string input);
	}
}