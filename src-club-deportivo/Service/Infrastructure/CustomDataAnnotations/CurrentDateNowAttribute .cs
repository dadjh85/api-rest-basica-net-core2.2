﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Service.Infrastructure.CustomDataAnnotations
{
    /// <summary>
    /// DataAnotation que verifica que la fecha sea superior a la actual
    /// </summary>
    public class CurrentDateNowAttribute : ValidationAttribute
    {
        /// <summary>
        /// Constructor del DataAnotation
        /// </summary>
        public CurrentDateNowAttribute()
        {
        }

        /// <summary>
        /// Método de validación
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            DateTime fecha = (DateTime)value;
            return fecha >= DateTime.UtcNow.AddHours(1);
        }
    }
}
