## API-REST BÁSICA EN .NET CORE 2.2
**Aspectos Importantes para a tener en cuenta antes de ejecutar el proyecto**

En la Carpeta: "ScriptsSQL" se encuentran cuatro archivos, tres de ellos para poder desplegar la base de datos.

1. modelo-ER.JPG: Esta imagen es el diagrama ER utilizado en la aplicación.
2. ScriptCreateDB.sql: Este Script te crearía la Base de datos con su estructura de tablas, también he creado otro Script solo con las tablas por si desean crear una base de datos por su cuenta y luego restaurar las tablas.
3. ScriptDBCreacionTablasSolo.sql: Este Script es el que comentaba anteriormente donde solo está la estructura de tablas de la aplicación.
4. ScriptDBDDatosTablas.sql: En este Script se encuentran todos los datos de las tablas introducidos en el desarrollo.

**Seguridad de la aplicación**

La API Se encuentra securizada por Token de acceso, la seguridad de la aplicación está preparada para que un determinado token pueda acceder a unas secciones de la API y otras no, solo he añadido un permiso en la URL: *GET: /api/deportes*, donde el primer token que voy a poner tendría acceso a toda la aplicación y el segundo token tendría acceso a todo menos a esa sección que le devuelve la API un error 403.

**Primer Token de Acceso (Permisos para acceder a toda las peticiones de la API)**

*Token:*
 eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2F1dGhzZXJ2ZXIuY29tIiwiaWF0IjoxNTUyMDg0NDEyLCJleHAiOjE1ODM2MjA0MTIsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ4Iiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJKdWFuIiwiQ2FuUmVhZFNwb3J0cyI6InRydWUifQ.OcJ1iTi8yPyQq46OQHQa0Q3FUoVJsvA2Pc7xAqc6ZEE

**Segundo Token de Acceso (Permisos a toda la aplicación menos a la petición: *GET: /api/deportes***

*Token:*

eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2F1dGhzZXJ2ZXIuY29tIiwiaWF0IjoxNTUyMDgzMTQwLCJleHAiOjE1ODM2MTkxNDAsImF1ZCI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzQ4Iiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJEYXZpZCJ9.cKfiw6GAwBm3Ev7Li4Xe3mC2jsUt3ygwujqC3xHiwoQ
 
 
**Aspectos a tener en cuenta en el uso de la API**

1. Todos los listados de la API se pueden paginar, para ello hay que rellenar los parámetros optativos en Swagger llamados page y pageSize, donde en el primer paramentro hay que ponerle el número de página y en el segundo el número total de elementos a mostrar en cada página, también se puede no rellenar esos parámetros y mostraría la totalidad de elementos de esa tabla.

2. Los parámetros de Fecha hay que pasarlos en el formato siguiente: "yyyy-mm-ddThh:mm:ss", por ejemplo: "2019-03-11T11:00:00", en la llamada a la petición de "obtener pistas disponibles cuya URL es: (GET: /api/Pistas/disponibles/{fecha}/{idUsuario}/{idDeporte})" hay que pasarle la fecha en ese formato ya que requiere fecha y hora, en caso de que el servicio requiera solo el campo fecha como en el caso de la petición de "Obtener listado de reservas del día cuya URL es: (GET: /api/Reservas/Dia/{fecha})" se le pasaría igual pero sin la parte de la hora, por ejemplo: 2019-03-11

3. Se han implementado las reglas de negocio a la hora de insertar o actualizar una reserva por lo que a la hora de probar eso hay que tener en cuenta que no se hagan más de tres reservas de un mismo usuario en un mismo día, más de dos reservas a la misma hora el mismo día por el mismo usuario o intentar realizar una reserva en una pista donde el día y la hora ya están ocupados, etc.

4. Esa regla de negocio también se utiliza en la obtención de pistas dispnibles.

5. Se han desarrollado unos 30 Test Unitarios donde se testea el funcionamiento de la API a nivel de Base de datos.


**Aspectos a tener en cuenta para la ejecución del proyecto**

* Para ejecutarlo es necesario compilar la aplicación con visual studio 2017, tal vez con versiones anteriores también se pueda, pero el desarrollo lo he realizado sobre dicha versión
* tener instalado el framework de net core 2.2 cuya url es la siguiente: https://dotnet.microsoft.com/download
* Y por supuesto tener restaurada la base de datos, y posteriormente cambiar la cadena de conexión de la base de datos del proyecto que como es lógico está en el archivo appsetting.json en el proyecto API que se llama: Club.Api
* También es necesario elegir como proyecto principal el proyecto de la API (Club.Api)
* El documento de JSON de swagger se puede visualizar al lanzar la aplicación en la siguiente URL: */swagger/v1/swagger.json* dicho enlace sale en la página principal del Swagger justo debajo del título de la aplicación.

**Descripción de las tablas de BD**

* Se han creado en total 5 Tablas:
	1. Usuario
	2. Socio
	3. Reserva
	4. Pista
	5. Deportes

* La tabla Deportes dispone de una relación de 1 a n con la tabla Pista, ya que Un deporte se puede practicar en numerosas pistas.

* A su vez la tabla Pista tiene una relación de 1 a n con la tabla Reserva, ya que una pista se puede reservar n veces.

* La tabla Usuario dispone de una relación 1 a 1 con la tabla socio, y a su vez la tabla Socio y Usuario disponen de una relación de 1 a n con la tabla Reservas.

En la imagen del Modelo ER se puede entender un poco mejor las relaciones que he detallado.
