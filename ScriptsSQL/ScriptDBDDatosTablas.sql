USE [ClubDeportivo]
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([Id], [Nombre], [Password], [Email], [Telefono], [Borrado]) VALUES (1, N'David Jiménez Hernández', N'CfDJ8MgEH6oF43NPhrgcy2kpnNGrMehX-b51_DLP3nIh29MPfMmiHWDY3j3hrkenyxqG45EQ-_0-eMYJWJHaqWKNmX4NM0du1Ts3TLgqdvin5y8jh-2U2T26_kSfgzYJiNkLuw', N'davidjimenez.t@gmail.com', N'+34968830158', 0)
INSERT [dbo].[Usuario] ([Id], [Nombre], [Password], [Email], [Telefono], [Borrado]) VALUES (2, N'Luis García', N'CfDJ8MgEH6oF43NPhrgcy2kpnNFW5QhF3gXOFiFWdaudqE7iduiBgFlatXZzH0_Wx4SYMJMI09bqsP0S5_I3kopJIalGpaUz0BbS_sI7TAwP8h3AHx3XPIbRLB1AP0fFEhVG2A', N'luisg@email.com', N'+34678956421', 0)
INSERT [dbo].[Usuario] ([Id], [Nombre], [Password], [Email], [Telefono], [Borrado]) VALUES (3, N'Pepe', N'CfDJ8MgEH6oF43NPhrgcy2kpnNEJRw3WfEvLOeU34CxA-EavhPXwxsZ7jiApzfSLWT9B6d97nQPg_i87QvtK2DyudkDnVAakXWA8ezqU8obq5fzeVIQRCu59QjYFidtyG1HU_A', N'pepe@email.com', N'+34618549682', 0)
INSERT [dbo].[Usuario] ([Id], [Nombre], [Password], [Email], [Telefono], [Borrado]) VALUES (4, N'Roberto', N'CfDJ8MgEH6oF43NPhrgcy2kpnNGs0WrDz3-U-U-P5PIsm0wi3JSHwwT1WGsempkiCp3MdvzH9C0mS2QxSLE_f9-mU_2nUEVKF2yS-7MiGUjGlJYS24wTcz6Z4-EMaN48M-9l8w', N'roberto@email.com', N'+34682564976', 0)
INSERT [dbo].[Usuario] ([Id], [Nombre], [Password], [Email], [Telefono], [Borrado]) VALUES (5, N'Alejandro', N'CfDJ8MgEH6oF43NPhrgcy2kpnNGAi1NLRLCOj-QtUO76-hENP1nIg0OqL4kvgrhiQ0jCguTnC1Yw2ZkPCPXYgx-_69e62lzpCHiRGDmGcDq5JfBHb0pRoU1ifEZoQ3M3oQpStg', N'alejandro@email.com', N'+34978654213', 0)
INSERT [dbo].[Usuario] ([Id], [Nombre], [Password], [Email], [Telefono], [Borrado]) VALUES (6, N'Ramón', N'CfDJ8MgEH6oF43NPhrgcy2kpnNFcdjTd8tU4kTFdYizscq4XwhyQpZvSWa8sxVCxVXArwx9b0hRiI3I9nYWvlOcBa1QN1lfjpcxJ5tzfPAtMIOb7k-fTlGdPHZyFT26oBgdoJQ', N'ramon@email.com', N'+34987456213', 0)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
SET IDENTITY_INSERT [dbo].[Socio] ON 

INSERT [dbo].[Socio] ([Id], [Nombre], [Email], [Telefono], [Direccion], [Poblacion], [CodigoPostal], [Borrado], [IdUsuario]) VALUES (17, N'David', N'dadjh85@gmail.com', N'+34617112919', N'Calle Mar Negro, 3', N'San Pedro del Pinatar', N'30740', 0, 1)
INSERT [dbo].[Socio] ([Id], [Nombre], [Email], [Telefono], [Direccion], [Poblacion], [CodigoPostal], [Borrado], [IdUsuario]) VALUES (18, N'Luis García', N'luis@email.com', N'+968545454', N'av. Livertad, 1', N'Murcia', N'30001', 0, 2)
INSERT [dbo].[Socio] ([Id], [Nombre], [Email], [Telefono], [Direccion], [Poblacion], [CodigoPostal], [Borrado], [IdUsuario]) VALUES (19, N'Pepe', N'pepe@email.com', N'+34968754859', N'Av. Constitución, 8', N'Murcia', N'30006', 0, 3)
INSERT [dbo].[Socio] ([Id], [Nombre], [Email], [Telefono], [Direccion], [Poblacion], [CodigoPostal], [Borrado], [IdUsuario]) VALUES (20, N'Roberto', N'roberto@email.com', N'+34687453215', N'Av. Nueva, 8', N'Alicante', N'03006', 0, 4)
INSERT [dbo].[Socio] ([Id], [Nombre], [Email], [Telefono], [Direccion], [Poblacion], [CodigoPostal], [Borrado], [IdUsuario]) VALUES (21, N'Alejandro', N'alejandro@email.com', N'+34687953216', N'Av. Vieja, 7', N'Elche', N'03001', 0, 5)
INSERT [dbo].[Socio] ([Id], [Nombre], [Email], [Telefono], [Direccion], [Poblacion], [CodigoPostal], [Borrado], [IdUsuario]) VALUES (22, N'Ramón', N'ramon@email.com', N'+3465483215', N'Av. de la universidad, 5', N'Alicante', N'03005', 0, 6)
SET IDENTITY_INSERT [dbo].[Socio] OFF
SET IDENTITY_INSERT [dbo].[Deportes] ON 

INSERT [dbo].[Deportes] ([Id], [Nombre], [NumeroJugadores]) VALUES (1, N'Fútbol', 22)
INSERT [dbo].[Deportes] ([Id], [Nombre], [NumeroJugadores]) VALUES (2, N'Tenis', 2)
INSERT [dbo].[Deportes] ([Id], [Nombre], [NumeroJugadores]) VALUES (3, N'Fútbol Sala', 10)
INSERT [dbo].[Deportes] ([Id], [Nombre], [NumeroJugadores]) VALUES (4, N'Voleibol', 10)
INSERT [dbo].[Deportes] ([Id], [Nombre], [NumeroJugadores]) VALUES (5, N'Baloncesto', 10)
SET IDENTITY_INSERT [dbo].[Deportes] OFF
SET IDENTITY_INSERT [dbo].[Pista] ON 

INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (6, N'Pista 1', 1, 1.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (7, N'Pista 2', 1, 2.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (8, N'Pista 3', 1, 2.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (9, N'Pista 4', 2, 3.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (10, N'Pista 5', 2, 3.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (11, N'Pista 6', 2, 3.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (12, N'Pista 7', 3, 1.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (13, N'Pista 8', 3, 1.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (14, N'Pista 9', 3, 1.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (15, N'Pista 10', 4, 1.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (16, N'Pista 11', 4, 1.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (17, N'Pista 12', 5, 4.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (18, N'Pista 13', 5, 4.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (19, N'Pista 14', 5, 4.5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (20, N'Pista 15', 5, 2)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (23, N'Pista 16', 1, 5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (24, N'Pista 17', 1, 5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (25, N'Pista 18', 1, 5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (26, N'Pista 19', 1, 5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (27, N'Pista 20', 1, 5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (28, N'Pista 21', 1, 5)
INSERT [dbo].[Pista] ([Id], [Nombre], [IdDeporte], [Precio]) VALUES (29, N'Pista 22', 1, 5)
SET IDENTITY_INSERT [dbo].[Pista] OFF
SET IDENTITY_INSERT [dbo].[Reserva] ON 

INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (9, CAST(N'2019-03-09T14:00:00' AS SmallDateTime), 6, 17, 2, NULL)
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (10, CAST(N'2019-03-09T15:00:00' AS SmallDateTime), 6, 17, 1, NULL)
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (12, CAST(N'2019-03-09T15:00:00' AS SmallDateTime), 7, 17, 2, NULL)
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (13, CAST(N'2019-03-09T16:00:00' AS SmallDateTime), 7, 17, 2, NULL)
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (16, CAST(N'2019-03-09T19:00:00' AS SmallDateTime), 6, 17, 1, NULL)
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (17, CAST(N'2019-03-10T19:00:00' AS SmallDateTime), 7, 17, 1, N'Poner Luz')
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (18, CAST(N'2019-03-10T19:00:00' AS SmallDateTime), 6, 17, 1, N'Poner Luz')
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (20, CAST(N'2019-03-10T13:00:00' AS SmallDateTime), 6, 17, 1, N'Test')
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (21, CAST(N'2019-03-10T14:00:00' AS SmallDateTime), 6, 18, 2, N'Test')
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (22, CAST(N'2019-03-10T15:00:00' AS SmallDateTime), 6, 19, 3, N'Test')
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (23, CAST(N'2019-03-10T16:00:00' AS SmallDateTime), 6, 20, 4, N'Test')
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (24, CAST(N'2019-03-10T17:00:00' AS SmallDateTime), 6, 21, 5, N'Test')
INSERT [dbo].[Reserva] ([Id], [Fecha], [IdPista], [IdSocio], [IdUsuario], [Observaciones]) VALUES (25, CAST(N'2019-03-10T18:00:00' AS SmallDateTime), 7, 22, 6, N'Test')
SET IDENTITY_INSERT [dbo].[Reserva] OFF
